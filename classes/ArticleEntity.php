<?php
class ArticleEntity
{
    protected $id;
    protected $title;
    protected $teaser;
    protected $thumbnail;
    protected $category;
    protected $content;
    protected $publishable_from;
    protected $publishable_to;
    /**
     * Accept an array of data matching properties of this class
     * and create the class
     *
     * @param array $data The data to use to create
     */
    public function __construct(array $data) {
        // no id if we're creating
        if(isset($data['id'])) {
            $this->id = $data['id'];
        }
        $this->title = $data['tittel'];
        $this->teaser = $data['ingress'];
        $this->thumbnail = $data['ingressbilde'];
        $this->category = $data['kategori'];
        $this->content = $data['innhold'];
        $this->publishable_from = $data['fra'];
        $this->publishable_to = $data['til'];
    }
    public function getFull(){
      return get_object_vars($this);
    }
    public function getPreview(){
      return (object) [
        'id' => $this->id,
        'title' => $this->title,
        'date' => $this->publishable_from,
        'teaser' => $this->teaser,
        'thumbnail' => $this->thumbnail,
        'category' => $this->category
      ];
    }
    public function getId() {
        return $this->id;
    }
    public function getTitle() {
        return $this->title;
    }
    public function getContent() {
        return $this->content;
    }
    public function getTeaser() {
        return $this->teaser;
    }
}
