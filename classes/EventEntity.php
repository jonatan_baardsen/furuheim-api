<?php
class EventEntity
{
    protected $id;
    protected $start;
    protected $end;
    protected $title;
    protected $description;
    protected $location;
    /**
     * Accept an array of data matching properties of this class
     * and create the class
     *
     * @param array $data The data to use to create
     */
    public function __construct(array $data) {
        // no id if we're creating
        if(isset($data['event_id'])) {
            $this->id = $data['event_id'];
        }
        $this->title = utf8_encode($data['event_title']);
        $this->description = $data['event_desc'];
        $this->location = utf8_encode($data['event_location']);
        $this->start = $this->parseDate($data['event_year'],$data['event_month'],$data['event_day'],$data['event_time']);
        $this->end = $this->parseDate($data['event_year'],$data['event_month'],$data['event_day'],$data['event_timeend']);
    }

    private function parseDate($year,$month,$day,$time){
      return date("c", strtotime($year.'-'.$month.'-'.$day.' '.$time.':00.000'));
    }

    public function getFull(){
      return get_object_vars($this);
    }
}
