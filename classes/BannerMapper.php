<?php

class BannerMapper extends Mapper
{
    public function getBanners() {
        $sql = "SELECT * FROM banner";

        $stmt = $this->db->prepare($sql);

        $stmt->execute();
        $results = [];
        while($row = $stmt->fetch()) {
            $banner = new BannerEntity($row);
            $results[] = $banner->getFull();
        }
        return $results;
    }
}
