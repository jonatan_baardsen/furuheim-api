<?php

class EventMapper extends Mapper
{
    public function getEvents($start,$end) {

        $sql = 'SELECT * FROM calendar_events where CAST(CONCAT(CONCAT(CONCAT(CONCAT(EVENT_YEAR, "-"), EVENT_MONTH), "-"), EVENT_DAY) AS DATE) BETWEEN CAST(:start AS DATE) AND CAST(:end AS DATE) ORDER BY EVENT_YEAR ASC, EVENT_MONTH ASC, EVENT_DAY ASC, event_time ASC';

        /*$sql = "SELECT * FROM calendar_events where EVENT_YEAR >= :startYear and EVENT_YEAR <= :endYear and EVENT_MONTH >= :startMonth and EVENT_MONTH <= :endMonth and EVENT_DAY >= :startDay and EVENT_DAY <= :endDay ORDER BY EVENT_YEAR ASC, EVENT_MONTH ASC, EVENT_DAY ASC, event_time ASC";*/



        $stmt = $this->db->prepare($sql);

        $stmt->bindParam(':start', $start->format('Y-n-j'), \PDO::PARAM_STR);
        $stmt->bindParam(':end', $end->format('Y-n-j'), \PDO::PARAM_STR);

        /*$stmt->bindParam(':startYear', intval($start->format('Y')), \PDO::PARAM_INT);
        $stmt->bindParam(':endYear', intval($end->format('Y')), \PDO::PARAM_INT);
        $stmt->bindParam(':startMonth', intval($start->format('n')), \PDO::PARAM_INT);
        $stmt->bindParam(':endMonth', intval($end->format('n')), \PDO::PARAM_INT);
        $stmt->bindParam(':startDay', intval($start->format('j')), \PDO::PARAM_INT);
        $stmt->bindParam(':endDay', intval($end->format('j')), \PDO::PARAM_INT);*/

        $stmt->execute();
        $results = [];
        while($row = $stmt->fetch()) {
            $event = new EventEntity($row);
            $results[] = $event->getFull();
        }
        return $results;
    }
}
