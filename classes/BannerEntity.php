<?php
class BannerEntity
{
    protected $id;
    protected $url;
    /**
     * Accept an array of data matching properties of this class
     * and create the class
     *
     * @param array $data The data to use to create
     */
    public function __construct(array $data) {
        // no id if we're creating
        $this->url = $data['sti'];
        $this->id = $data['id'];
    }

    public function getFull(){
      return get_object_vars($this);
    }
}
