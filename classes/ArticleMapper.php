<?php

class ArticleMapper extends Mapper
{
    private $articles_per_page = 15;
    public function getArticles($page = 1, $category) {
        if($page == null) { $page = 1; }
        $calculatedPage = $page * $this->articles_per_page - $this->articles_per_page;
        if(isset($category) && !empty($category)) {
          $sql = "SELECT * FROM artikler WHERE publisert=2 AND menighet=0 AND fra <= :date AND kategori=:category ORDER BY fra DESC, id DESC LIMIT :from,:amount";
        }else{
          $sql = "SELECT * FROM artikler WHERE publisert=2 AND menighet=0 AND fra <= :date ORDER BY fra DESC, id DESC LIMIT :from,:amount";
        }

        $stmt = $this->db->prepare($sql);

        $stmt->bindParam(':date', date('Y-m-d'), \PDO::PARAM_STR);
        $stmt->bindParam(':from', $calculatedPage, \PDO::PARAM_INT);
        if(isset($category) && !empty($category)) {
          $stmt->bindParam(':category', $category, \PDO::PARAM_STR);
        }
        $stmt->bindParam(':amount', $this->articles_per_page, \PDO::PARAM_INT);
        $stmt->execute();
        $results = [];
        while($row = $stmt->fetch()) {
            $article = new ArticleEntity($row);
            $results[] = $article->getPreview();
        }
        return $results;
    }
    /**
     * Get one ticket by its ID
     *
     * @param int $ticket_id The ID of the ticket
     * @return TicketEntity  The ticket
     */
    public function getArticleById($article_id) {
        $sql = "SELECT * FROM artikler WHERE id = ".$article_id;
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute(["id" => $article_id]);
        if($result) {
            $article = new ArticleEntity($stmt->fetch());
            return $article->getFull();
        }
    }
    public function save(TicketEntity $ticket) {
        $sql = "insert into tickets
            (title, description, component_id) values
            (:title, :description,
            (select id from components where component = :component))";
        $stmt = $this->db->prepare($sql);
        $result = $stmt->execute([
            "title" => $ticket->getTitle(),
            "description" => $ticket->getDescription(),
            "component" => $ticket->getComponent(),
        ]);
        if(!$result) {
            throw new Exception("could not save record");
        }
    }
}
