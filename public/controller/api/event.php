<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/events', function (Request $request, Response $response) {

    $start = (new DateTime())->setTimestamp(date('U',strtotime($request->getQueryParam('start',(new DateTime())->format('c')))));
    $start_clone = clone $start;
    $end = (new DateTime())->setTimestamp(date('U',strtotime($request->getQueryParam('end',$start_clone->add(new DateInterval('P1W'))->format('c')))));


    $mapper = new EventMapper($this->db);
    $events = $mapper->getEvents($start,$end);

    return $response->withJSON($events);
});

?>
