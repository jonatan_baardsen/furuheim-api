<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/articles[/{page}]', function (Request $request, Response $response) {
    $page = $request->getAttribute('page');
    $category = $request->getQueryParam('category');

    $mapper = new ArticleMapper($this->db);
    $articles = $mapper->getArticles($page,$category);


    return $response->withJSON($articles);;
});

$app->get('/api/article/{id}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $mapper = new ArticleMapper($this->db);
    $article = $mapper->getArticleById($id);

    return $response->withJSON($article);
});

$app->post('/article', function (Request $request, Response $response) {
  
});

?>
