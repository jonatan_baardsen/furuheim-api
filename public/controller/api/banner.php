<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/banners', function (Request $request, Response $response) {

    $mapper = new BannerMapper($this->db);
    $banners = $mapper->getBanners();

    return $response->withJSON($banners);
});

$app->post('/api/banners',function(Request $request,Response $response){

	//$requestVariables = $request->getParsedBody();
	//$url = $requestVariables['url'];
/*
TODO:
*/
	//return $response->withJSON($url);
	return null;
});

?>
