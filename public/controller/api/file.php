<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Aws\S3\S3Client as Client;

$sharedConfig = [
    'region'  => 'eu-central-1',
    'version' => 'latest'
];

$client = Client::factory($sharedConfig);
$keyid = getenv("AWS_ACCESS_KEY_ID");
$keyvalue = getenv("AWS_SECRET_ACCESS_KEY");
$bucket = "furuheim";

$sdk = new Aws\Sdk($sharedConfig);

$app->get('/api/file/sign/[{id}]', function (Request $request, Response $response) {
	global $client,$bucket;
	//Gives the file an uniq id
	$filename = uniqid() . "." .pathinfo($request->getAttribute("id"),PATHINFO_EXTENSION);//uniqid() . "." . end($splittedId);
  
	$cmd = $client->getCommand("PutObject",[
		'Bucket' => $bucket,
		'Key' => $filename
		]);
	$AWSrequest = $client->createPresignedRequest($cmd,"+5 minutes");
	$presignedUrl = (string) $AWSrequest->getUri();
    return $response->withJSON(array(
    	"url" => $presignedUrl,
    	"filename" => $filename
    	));
});

$app->get('/api/file/[{id}]', function (Request $request, Response $response) {
	global $client,$bucket;

	$cmd = $client->getCommand("GetObject",[
		'Bucket' => $bucket,
		'Key' => $request->getAttribute("id")
		]);
	$AWSrequest = $client->createPresignedRequest($cmd,"+5 minutes");
	$presignedUrl = (string) $AWSrequest->getUri();
    return $response->withStatus(200)->withHeader("Location",$presignedUrl);
});

$app->delete('/api/file/[{id}]', function (Request $request,Response $response){
	global $client,$bucket;

	$result = $client->deleteObject([
		'Bucket' => $bucket,
		'Key' => $request->getAttribute('id')
		]);
return $response->withJSON($result);
});

?>
