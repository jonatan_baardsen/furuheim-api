<?php

require_once(dirname(__FILE__) . "/lib/SherwoodSignOn/Autoloader.php");
SherwoodSignOn_Autoloader::getInstance()->register();

$client = new SherwoodSignOn_Client();

$ticket = isset($_GET['ticket']) ? $_GET['ticket'] : "";
$signature = isset($_GET['signature']) ? $_GET['signature'] : "";
$returnurl = isset($_GET['returnurl']) ? $_GET['returnurl'] : "/";

if ($client->clientSignOn($ticket, $signature)) {
	if ($returnurl != "") {
		header("Status: 302 Found");
		header("Location: $returnurl");
		die();
	}
}
