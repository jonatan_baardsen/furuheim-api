<?php

require_once dirname(dirname(__FILE__)) . "/lib/SherwoodSignOn/Autoloader.php";
SherwoodSignOn_Autoloader::getInstance()->register();

abstract class SherwoodSignOn_Tests_DatabaseTestCase extends PHPUnit_Extensions_Database_TestCase {

	/**
	 * only instantiate pdo once for test clean-up/fixture load
	 * @var PDO
	 */
	static private $pdo = null;

	/**
	 * only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
	 * @var PHPUnit_Extensions_Database_DB_IDatabaseConnection
	 */
	private $conn = null;

	/**
	 * Returns the test dataset.
	 *
	 * @return PHPUnit_Extensions_Database_DataSet_IDataSet
	 */
	protected function getDataSet() {

		ob_start();
		include dirname(__FILE__) . '/data/sessionRepository.yml';

		return new PHPUnit_Extensions_Database_DataSet_YamlDataSet(
			ob_get_clean()
		);
	}

	/**
	 * @return PHPUnit_Extensions_Database_DB_IDatabaseConnection
	 */
	final public function getConnection() {

		if ($this->conn === null) {

			if (self::$pdo === null) {
				self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);

				// Drop if it already exists
				self::$pdo->query("DROP TABLE SsoClientSessions");

				self::$pdo->query("CREATE TABLE SsoClientSessions (
				  sessionid varchar(100) NOT NULL,
				  activated tinyint(1) DEFAULT NULL,
				  clientidentifier varchar(255) DEFAULT NULL,
				  datecreated datetime DEFAULT NULL,
				  datechecked datetime DEFAULT NULL,
				  PRIMARY KEY (sessionid)
				)");
			}

			$this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
		}

		return $this->conn;
	}
}