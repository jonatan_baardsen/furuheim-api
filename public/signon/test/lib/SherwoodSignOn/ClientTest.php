<?php

class SherwoodSignOn_ClientTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var SherwoodSignOn_Client
	 */
	protected $object;

    /**
     * @var SherwoodSignOn_Configuration_Xml
     */
    protected $_config;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new SherwoodSignOn_Client();
		$this->object->debug = true;
		$this->_config = new SherwoodSignOn_Configuration_Xml(dirname(dirname(dirname(__FILE__))) . "/data/SSOClientSettings.xml");
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
	}

	/**
	 */
	public function testSetUserProfileRepository() {
		$reflection_class = new ReflectionClass("SherwoodSignOn_Client");
		$property = $reflection_class->getProperty('_userProfileRepository');
		$property->setAccessible(true);

		$this->assertEquals(null, $property->getValue($this->object));

		$oldDocumentRoot = $_SERVER['DOCUMENT_ROOT'];
		$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(dirname(__FILE__))). '/data/doc_root';

		$instance = $this->getMock('SherwoodSignOn_UserProfileRepository_Session', array('setClientIdentifier'));
		/** @var $instance PHPUnit_Framework_MockObject_MockObject|SherwoodSignOn_UserProfileRepository_Session */
		$instance->expects($this->once())->method('setClientIdentifier')->with($this->equalTo($this->_config->getClientCode()));

		$this->object->setUserProfileRepository($instance);
		$this->assertEquals($instance, $property->getValue($this->object));

		$_SERVER['DOCUMENT_ROOT'] = $oldDocumentRoot;
	}

	/**
	 */
	public function testGetUserProfileRepository() {
		$reflection_class = new ReflectionClass("SherwoodSignOn_Client");
		$property = $reflection_class->getProperty('_userProfileRepository');
		$property->setAccessible(true);

		$this->assertEquals(null, $property->getValue($this->object));

		$oldDocumentRoot = $_SERVER['DOCUMENT_ROOT'];
		$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(dirname(__FILE__))). '/data/doc_root';

		$result = $this->object->getUserProfileRepository();
		$this->assertEquals($property->getValue($this->object), $result);

		$_SERVER['DOCUMENT_ROOT'] = $oldDocumentRoot;

		$instance = $this->getMock('SherwoodSignOn_UserProfileRepository_Session');
		$property->setValue($this->object, $instance);
		$this->assertEquals($instance, $this->object->getUserProfileRepository());
	}

	/**
	 */
	public function testSetSessionRepository() {
		$this->object->setClientSettings($this->_config);

		$reflection_class = new ReflectionClass("SherwoodSignOn_Client");
		$property = $reflection_class->getProperty('_sessionRepository');
		$property->setAccessible(true);

		$this->assertEquals(null, $property->getValue($this->object));

		$instance = $this->getMock('SherwoodSignOn_SessionRepository_MySql', array('setConfiguration'));
		/** @var $instance PHPUnit_Framework_MockObject_MockObject|SherwoodSignOn_SessionRepository_MySql*/
		$instance->expects($this->never())->method('setConfiguration');

		$this->object->setSessionRepository($instance, false);
		$this->assertEquals($instance, $property->getValue($this->object));

		$instance = $this->getMock('SherwoodSignOn_SessionRepository_MySql', array('setConfiguration'));
		$instance->expects($this->once())->method('setConfiguration')->with($this->equalTo($this->_config->getSessionRepositoryTimeout()),
			$this->equalTo($this->_config->getClientCode()), $this->equalTo($this->_config->getSessionRepositorySettings()));

		$this->object->setSessionRepository($instance);
		$this->assertEquals($instance, $property->getValue($this->object));
	}

	/**
	 * @expectedException		InvalidArgumentException
	 * @expectedExceptionMessage UnknownRepository
	 *
	 */
	public function testGetSessionRepository() {
		$this->object->setClientSettings($this->_config);

		$reflection_class = new ReflectionClass("SherwoodSignOn_Client");
		$property = $reflection_class->getProperty('_sessionRepository');
		$property->setAccessible(true);

		$this->assertEquals(null, $property->getValue($this->object));

		// Run test for partical class-name
		$instance = $this->object->getSessionRepository();
		$this->assertEquals($property->getValue($this->object), $instance);
		$this->assertEquals(get_class($instance), 'SherwoodSignOn_SessionRepository_Pdo');

		// Run Test for simple full class-name
        /** @var PHPUnit_Framework_MockObject_MockObject|SherwoodSignOn_Configuration_Xml $_config */
        $_config = $this->getMock("SherwoodSignOn_Configuration_Xml", array("getSessionRepositoryType"), array(dirname(dirname(dirname(__FILE__))) . "/data/SSOClientSettings.xml"));
        $_config->expects($this->once())
            ->method('getSessionRepositoryType')
            ->will($this->returnValue("SherwoodSignOn_SessionRepository_Memcache"));
        $this->object->setClientSettings($_config);

		$property->setValue($this->object, null);
		$instance = $this->object->getSessionRepository();
		$this->assertEquals($property->getValue($this->object), $instance);
		$this->assertEquals(get_class($instance), 'SherwoodSignOn_SessionRepository_Memcache');

		// Run Test for Exception
        /** @var PHPUnit_Framework_MockObject_MockObject|SherwoodSignOn_Configuration_Xml $_config */
        $_config = $this->getMock("SherwoodSignOn_Configuration_Xml", array("getSessionRepositoryType"), array(dirname(dirname(dirname(__FILE__))) . "/data/SSOClientSettings.xml"));
        $_config->expects($this->once())
            ->method('getSessionRepositoryType')
            ->will($this->returnValue("UnknownRepository"));
		$this->object->setClientSettings($_config);

		$property->setValue($this->object, null);
		$this->object->getSessionRepository();
	}

	/**
	 */
	public function testSetClientSettings() {
		$reflection_class = new ReflectionClass("SherwoodSignOn_Client");
		$property = $reflection_class->getProperty('_config');
		$property->setAccessible(true);

		$this->assertEquals(null, $property->getValue($this->object));

		$instance = $this->getMockBuilder('SherwoodSignOn_Configuration_Xml')->disableOriginalConstructor()->getMock();
		/** @var $instance SherwoodSignOn_Configuration_Xml */
		$this->object->setClientSettings($instance);
		$this->assertEquals($instance, $property->getValue($this->object));
	}

	/**
	 */
	public function testGetClientSettings() {
		$reflection_class = new ReflectionClass("SherwoodSignOn_Client");
		$property = $reflection_class->getProperty('_config');
		$property->setAccessible(true);

		$this->assertEquals(null, $property->getValue($this->object));

		$oldDocumentRoot = $_SERVER['DOCUMENT_ROOT'];
		$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(dirname(__FILE__))) . '/data/doc_root';

		$result = $this->object->getClientSettings();
		$this->assertEquals($property->getValue($this->object), $result);

		$instance = $this->getMock('SherwoodSignOn_Configuration');
		$property->setValue($this->object, $instance);
		$this->assertEquals($instance, $this->object->getClientSettings());

		$_SERVER['DOCUMENT_ROOT'] = $oldDocumentRoot;
	}
}
