<?php

class SherwoodSignOn_AutoloaderTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var SherwoodSignOn_Autoloader
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = SherwoodSignOn_Autoloader::getInstance();
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
	}

	public function testLoadClass() {
		$this->assertFalse(class_exists('SherwoodSignOn_Client', false));
		$this->assertTrue($this->object->loadClass('SherwoodSignOn_Client'));
		$this->assertTrue(class_exists('SherwoodSignOn_Client', false));

		$this->assertFalse($this->object->loadClass('UnexistingClass'));
	}

	/**
	 * Even if this test does not pass in PHP 5.2.2, it works.
	 */
	public function testRegister() {
		$this->object->register();

		$autoloaderList = spl_autoload_functions();
		$lastAutoloader = array_pop($autoloaderList);
		$this->assertEquals('SherwoodSignOn_Autoloader', get_class($lastAutoloader[0]));
		$this->assertEquals('loadClass', $lastAutoloader[1]);
	}
}
