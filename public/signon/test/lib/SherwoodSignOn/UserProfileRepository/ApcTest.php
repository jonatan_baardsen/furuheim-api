<?php

class SherwoodSignOn_UserProfileRepository_ApcTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var PHPUnit_Framework_MockObject_MockObject|SherwoodSignOn_UserProfileRepository_Apc
	 */
	protected $object;

	/**
	 * @var string
	 */
	private $clientCode = "UnitTest";

	private $anonymousUuid;
	private $anonymousUuidKey;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		if (!function_exists("apc_add"))
			$this->markTestSkipped("APC is not installed as PHP-extension");

		$this->object = new SherwoodSignOn_UserProfileRepository_Apc();
		$this->object->setClientIdentifier($this->clientCode);

		$this->anonymousUuid = '00000000-0000-0000-0000-000000000000';
		$this->anonymousUuidKey = SherwoodSignOn_UserProfileRepository_Interface::uniqueKey . '|' . $this->clientCode . '|' . $this->anonymousUuid;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		if (function_exists("apc_add"))
			apc_clear_cache('user');
	}

	public function testSetUserProfile() {
		$instance = new SherwoodSignOn_UserProfile();
		$this->assertTrue($this->object->setUserProfile($instance));

		$this->assertEquals($instance, apc_fetch($this->anonymousUuidKey));
	}

	public function testGetUserProfile() {
		apc_store($this->anonymousUuidKey, new SherwoodSignOn_UserProfile());
		$this->assertEquals(apc_fetch($this->anonymousUuidKey), $this->object->getUserProfile($this->anonymousUuid));

		apc_store($this->anonymousUuidKey, '');
		$this->assertEquals(apc_fetch($this->anonymousUuidKey), $this->object->getUserProfile($this->anonymousUuid));

		apc_delete($this->anonymousUuidKey);
		$this->assertFalse($this->object->getUserProfile($this->anonymousUuid));
	}

	public function testDeleteUserProfile() {
		// return true even if nothing's changed becasue the profile was deleted.
		$this->assertTrue($this->object->deleteUserProfile($this->anonymousUuid));

		apc_store($this->anonymousUuidKey, new SherwoodSignOn_UserProfile());
		$this->assertTrue($this->object->deleteUserProfile($this->anonymousUuid));
		$this->assertFalse(apc_fetch($this->anonymousUuidKey));

		apc_store($this->anonymousUuidKey, '');
		$this->assertTrue($this->object->deleteUserProfile($this->anonymousUuid));
		$this->assertFalse(apc_fetch($this->anonymousUuidKey));
	}
}
