<?php

class SherwoodSignOn_UserProfileRepository_SessionTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var PHPUnit_Framework_MockObject_MockObject|SherwoodSignOn_UserProfileRepository_Session
	 */
	protected $object;

	/**
	 * @var PHPUnit_Framework_MockObject_MockObject|SherwoodSignOn_UserProfileRepository_Session
	 */
	protected $object2;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = $this->getMock('SherwoodSignOn_UserProfileRepository_Session', array('startSession'));
		$this->object2 = clone $this->object;

		$this->object->expects($this->any())
				->method('startSession')
				->will($this->returnValue(true));
		$this->object2->expects($this->any())
				->method('startSession')
				->will($this->returnValue(false));
		$_SESSION = array();
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		$_SESSION = array();
	}

	public function testSetUserProfile() {
		$instance = new SherwoodSignOn_UserProfile();
		$this->assertTrue($this->object->setUserProfile($instance));

		$this->assertEquals(array('profile' => serialize($instance), 'saveTime' => time()), $_SESSION[SherwoodSignOn_UserProfileRepository_Interface::uniqueKey]);

		$this->assertFalse($this->object2->setUserProfile($instance));
	}

	public function testGetUserProfile() {
		$profile = new SherwoodSignOn_UserProfile();
		$_SESSION[SherwoodSignOn_UserProfileRepository_Interface::uniqueKey] = array('profile' => serialize($profile), 'saveTime' => time());
		$this->assertEquals($profile, $this->object->getUserProfile('00000000-0000-0000-0000-000000000000'));

		$_SESSION[SherwoodSignOn_UserProfileRepository_Interface::uniqueKey] = "";
		$this->assertEquals($_SESSION[SherwoodSignOn_UserProfileRepository_Interface::uniqueKey], $this->object->getUserProfile('00000000-0000-0000-0000-000000000000'));
		$this->assertFalse($this->object2->getUserProfile('00000000-0000-0000-0000-000000000000'));

		unset($_SESSION[SherwoodSignOn_UserProfileRepository_Interface::uniqueKey]);
		$this->assertFalse($this->object->getUserProfile('00000000-0000-0000-0000-000000000000'));
	}

	public function testDeleteUserProfile() {
		// return true even if nothing's changed becasue the profile was deleted.
		$this->assertTrue($this->object->deleteUserProfile('00000000-0000-0000-0000-000000000000'));

		$_SESSION[SherwoodSignOn_UserProfileRepository_Interface::uniqueKey] = new SherwoodSignOn_UserProfile();
		$this->assertTrue($this->object->deleteUserProfile('00000000-0000-0000-0000-000000000000'));
		$this->assertFalse(isset($_SESSION[SherwoodSignOn_UserProfileRepository_Interface::uniqueKey]));

		$_SESSION[SherwoodSignOn_UserProfileRepository_Interface::uniqueKey] = "";
		$this->assertTrue($this->object->deleteUserProfile('00000000-0000-0000-0000-000000000000'));
		$this->assertFalse(isset($_SESSION[SherwoodSignOn_UserProfileRepository_Interface::uniqueKey]));

		$this->assertFalse($this->object2->deleteUserProfile('00000000-0000-0000-0000-000000000000'));
	}
}
