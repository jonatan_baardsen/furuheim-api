<?php

class SherwoodSignOn_UserProfileTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var SherwoodSignOn_UserProfile
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
		$xml = '<getUserProfileResponse>
					<userAccountId></userAccountId>
					<username>testuser</username>
					<firstName></firstName>
					<lastName></lastName>
					<country></country>
					<dateOfBirth></dateOfBirth>
					<gender>0</gender>
					<email></email>
					<globalUser>false</globalUser>
					<role name="unregistered_group" source="test" />
					<role name="Group3" source="test" />
					<role name="Group3" source="abc" />
					<attribute name="Attribute1" source="test" value="Val1" />
					<attribute name="Attribute2" source="test" value="Val2" />
					<attribute name="Attribute2" source="abc" value="Val2" />
				</getUserProfileResponse>';
        $this->object = new SherwoodSignOn_UserProfile(simplexml_load_string($xml));
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

	public function testAttributes() {
		$this->assertEquals('Val1', $this->object->attributes['test:Attribute1']);
		$this->assertEquals('Val2', $this->object->attributes['test:Attribute2']);
		$this->assertEquals('Val2', $this->object->attributes['abc:Attribute2']);
	}

	public function testRoles() {
		$this->assertTrue(in_array('test:unregistered_group', $this->object->roles));
		$this->assertTrue(in_array('test:Group3', $this->object->roles));
		$this->assertTrue(in_array('abc:Group3', $this->object->roles));
	}
}
?>
