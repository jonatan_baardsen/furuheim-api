<?php

class SherwoodSignOn_SessionRepository_ApcTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var SherwoodSignOn_SessionRepository_Apc
	 */
	protected $object;

	/**
	 * @var string
	 */
	private $clientCode = "UnitTest";

	/**
	 * @var string
	 */
	private $config;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		if (!function_exists("apc_add"))
			$this->markTestSkipped("APC is not installed as PHP-extension");

		$this->object = new SherwoodSignOn_SessionRepository_Apc();
		$this->object->setConfiguration(5, $this->clientCode);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		if (function_exists("apc_add"))
			apc_clear_cache('user');
	}

	public function testSetConfiguration() {
		$reflection_class = new ReflectionClass('SherwoodSignOn_SessionRepository_Apc');
		$propertySessionTimeoutInMinutes = $reflection_class->getProperty('sessionTimeoutInMinutes');
		$propertySessionTimeoutInMinutes->setAccessible(true);

		$propertyClientIdentifier = $reflection_class->getProperty('clientIdentifier');
		$propertyClientIdentifier->setAccessible(true);

		$instance = new SherwoodSignOn_SessionRepository_Apc();

		$this->assertEquals(null, $propertySessionTimeoutInMinutes->getValue($instance));
		$this->assertEquals(null, $propertyClientIdentifier->getValue($instance));

		$instance->setConfiguration(5, $this->clientCode);

		$this->assertEquals(5, $propertySessionTimeoutInMinutes->getValue($instance));
		$this->assertEquals($this->clientCode, $propertyClientIdentifier->getValue($instance));
	}

	public function testCreateSession() {
		$sessionId = $this->object->createSession();

		$session = apc_fetch(SherwoodSignOn_SessionRepository_Interface::uniqueKey . '|' . $this->clientCode . "|" . $sessionId);
		$this->assertEquals("0", $session);
	}

	public function testActivateSession() {
		$sessionId = $this->object->createSession();

		// Do I get TRUE if the session is acitvated?
		$result = $this->object->activateSession($sessionId);
		$this->assertEquals(true, $result);

		// Is the session really activated?
		$session = apc_fetch(SherwoodSignOn_SessionRepository_Interface::uniqueKey . '|' . $this->clientCode . "|" . $sessionId);
		$this->assertEquals("1", $session);

		// What about an non-existing or expired session-id?
		$result = $this->object->activateSession($sessionId . "2");
		$this->assertEquals(false, $result);
	}

	public function testUpdateSession() {
		$sessionId = $this->object->createSession();

		// You should not be able to update a non-activated session.
		$result = $this->object->updateSession($sessionId);
		$this->assertEquals(false, $result);

		$this->object->activateSession($sessionId);

		$reflection_class = new ReflectionClass("SherwoodSignOn_SessionRepository_Apc");
		$property = $reflection_class->getProperty('sessionTimeoutInMinutes');
		$property->setAccessible(true);

		// Update Session with new time (1 sec)
		$property->setValue($this->object, 1 / 60);
		$result = $this->object->updateSession($sessionId);
		$this->assertEquals(true, $result);

		// Reading the session should work.
		$session = apc_fetch(SherwoodSignOn_SessionRepository_Interface::uniqueKey . '|' . $this->clientCode . "|" . $sessionId);
		$this->assertEquals("1", $session);
	#	sleep(1);
	#	// After one sec. the session should be expired.
	#	$session = apc_fetch($this->clientCode . "|" . $sessionId);
	#	$this->assertEquals(false, $session);
	# Untestable because values are first deleted on the next request.
	}

	public function testDeleteSession() {
		$sessionId = $this->object->createSession();
		$this->object->deleteSession($sessionId);

		$session = apc_fetch(SherwoodSignOn_SessionRepository_Interface::uniqueKey . '|' . $this->clientCode . "|" . $sessionId);
		$this->assertEquals(false, $session);
	}

	public function testCleanUp() {
		// Nothing to test
	}

    /**
     * Real-life stress-test
     *
     * Try to activate and update a session quite shortly after it's created
     * Some SQL engines do not return the count of found data, but effectively updated data. This leads to strange
     * behaviour here, since we early counted the updated rows. But the only data that changed was the date in seconds.
     * But if the updating of a session was executed less than a second after it's updating, this failed.
     */
    public function testCreateActivateAndUpdateSession() {
        $session = $this->object->createSession();
        $this->assertTrue($this->object->activateSession($session));
        $this->assertTrue($this->object->updateSession($session));
    }
}
