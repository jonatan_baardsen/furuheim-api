<?php

class SherwoodSignOn_SessionRepository_PdoTest extends SherwoodSignOn_Tests_DatabaseTestCase {

	/**
	 * @var SherwoodSignOn_SessionRepository_Pdo
	 */
	protected $object;

	/**
	 * @var string
	 */
	private $config;

	/**
	 * @var string
	 */
	private $clientCode = "UnitTest";

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		parent::setUp();

		$reflection_class = new ReflectionClass("SherwoodSignOn_SessionRepository_Pdo");

		$this->object = new SherwoodSignOn_SessionRepository_Pdo();

		// Important for using sqlite::memory: for testing
		$propertyPdo = $reflection_class->getProperty('pdo');
		$propertyPdo->setAccessible(true);
		$propertyPdo->setValue($this->object, $this->getConnection()->getConnection());
		$propertyPdo->setAccessible(false);

        $this->object->setConfiguration(5, $this->clientCode, array(
            'dsn' => $GLOBALS['DB_DSN'],
            'username' => $GLOBALS['DB_USER'],
            'password' => $GLOBALS['DB_PASSWD']
        ));
	}

	public function testSetConfiguration() {
		$reflection_class = new ReflectionClass("SherwoodSignOn_SessionRepository_Pdo");
		$propertySessionTimeoutInMinutes = $reflection_class->getProperty('sessionTimeoutInMinutes');
		$propertySessionTimeoutInMinutes->setAccessible(true);

		$propertyClientIdentifier = $reflection_class->getProperty('clientIdentifier');
		$propertyClientIdentifier->setAccessible(true);

		$propertyDsn = $reflection_class->getProperty('_dsn');
		$propertyDsn->setAccessible(true);

		$propertyUsername = $reflection_class->getProperty('_username');
		$propertyUsername->setAccessible(true);

		$propertyPassword = $reflection_class->getProperty('_password');
		$propertyPassword->setAccessible(true);

		$instance = new SherwoodSignOn_SessionRepository_Pdo();

		$this->assertEquals(null, $propertySessionTimeoutInMinutes->getValue($instance));
		$this->assertEquals(null, $propertyClientIdentifier->getValue($instance));
		$this->assertEquals(null, $propertyDsn->getValue($instance));
		$this->assertEquals(null, $propertyUsername->getValue($instance));
		$this->assertEquals(null, $propertyPassword->getValue($instance));

		$instance->setConfiguration(5, $this->clientCode, array(
            'dsn' => $GLOBALS['DB_DSN'],
            'username' => $GLOBALS['DB_USER'],
            'password' => $GLOBALS['DB_PASSWD']
        ));

		$this->assertEquals(5, $propertySessionTimeoutInMinutes->getValue($instance));
		$this->assertEquals($this->clientCode, $propertyClientIdentifier->getValue($instance));
		$this->assertEquals($GLOBALS['DB_DSN'], $propertyDsn->getValue($instance));
		$this->assertEquals($GLOBALS['DB_USER'], $propertyUsername->getValue($instance));
		$this->assertEquals($GLOBALS['DB_PASSWD'], $propertyPassword->getValue($instance));
	}

	public function testCreateSession() {
		$oldCount = $this->getConnection()->getRowCount('SsoClientSessions');

		$session = $this->object->createSession();

		$newRow = $this->getConnection()->createQueryTable('SsoClientSessions_Create',
			'SELECT sessionid, activated, clientidentifier, datechecked  FROM SsoClientSessions ORDER BY datecreated DESC')
				->getRow(0);

		$this->assertEquals(
			array(
				 'sessionid' => $session,
				 'activated' => 0,
				 'clientidentifier' => $this->clientCode,
				 'datechecked' => NULL
			),
			$newRow
		);

		$this->assertEquals($oldCount + 1, $this->getConnection()->getRowCount('SsoClientSessions'));
	}

	public function testActivateSession() {
		$oldCount = $this->getConnection()->getRowCount('SsoClientSessions');

		$this->assertFalse($this->object->activateSession('sessiontest1'));
		$this->assertFalse($this->object->activateSession('sessiontest3'));
		$this->assertFalse($this->object->activateSession('sessiontest4'));
		$this->assertFalse($this->object->activateSession('sessiontest5'));

		$queryTable = $this->getConnection()->createQueryTable('SsoClientSessions_Create',
			'SELECT * FROM SsoClientSessions WHERE sessionid LIKE
				\'sessiontest%\' ORDER BY sessionid ASC');

		$schema = $this->getDataSet();

		$this->assertEquals(
			$schema->getTable('SsoClientSessions')->getRow(0),
			$queryTable->getRow(0)
		);

		$this->assertEquals(
			$schema->getTable('SsoClientSessions')->getRow(1),
			$queryTable->getRow(1)
		);

		$this->assertEquals(
			$schema->getTable('SsoClientSessions')->getRow(2),
			$queryTable->getRow(2)
		);

		$this->assertEquals(
			$schema->getTable('SsoClientSessions')->getRow(3),
			$queryTable->getRow(3)
		);

		$this->assertEquals(
			$schema->getTable('SsoClientSessions')->getRow(4),
			$queryTable->getRow(4)
		);

		$session = $this->object->createSession();
		$this->assertTrue($this->object->activateSession($session));

		$newRow = $this->getConnection()->createQueryTable('SsoClientSessions_Create',
			'SELECT * FROM SsoClientSessions ORDER BY datecreated DESC')
				->getRow(0);

		$this->assertNotEmpty($newRow['datecreated']);
		unset($newRow['datecreated']);
		$this->assertNotEmpty($newRow['datechecked']);
		unset($newRow['datechecked']);

		$this->assertEquals(
			array(
				 'sessionid' => $session,
				 'activated' => 1,
				 'clientidentifier' => $this->clientCode
			),
			$newRow
		);

		$this->assertEquals($oldCount + 1, $this->getConnection()->getRowCount('SsoClientSessions'));
	}

	public function testUpdateSession() {
		$oldCount = $this->getConnection()->getRowCount('SsoClientSessions');

		$this->assertFalse($this->object->updateSession('sessiontest1'));
		$this->assertFalse($this->object->updateSession('sessiontest2'));
		$this->assertFalse($this->object->updateSession('sessiontest4'));
		$this->assertFalse($this->object->updateSession('sessiontest5'));

		$queryTable = $this->getConnection()->createQueryTable('SsoClientSessions_Create',
			'SELECT * FROM SsoClientSessions WHERE sessionid LIKE
				\'sessiontest%\' ORDER BY sessionid ASC');

		$schema = $this->getDataSet();

		$this->assertEquals(
			$schema->getTable('SsoClientSessions')->getRow(0),
			$queryTable->getRow(0)
		);

		$this->assertEquals(
			$schema->getTable('SsoClientSessions')->getRow(1),
			$queryTable->getRow(1)
		);

		$this->assertEquals(
			$schema->getTable('SsoClientSessions')->getRow(2),
			$queryTable->getRow(2)
		);

		$this->assertEquals(
			$schema->getTable('SsoClientSessions')->getRow(3),
			$queryTable->getRow(3)
		);

		$this->assertEquals(
			$schema->getTable('SsoClientSessions')->getRow(4),
			$queryTable->getRow(4)
		);

		$session = $this->object->createSession();
		$this->object->activateSession($session);
		sleep(1);
		$this->assertTrue($this->object->updateSession($session));

		$newRow = $this->getConnection()->createQueryTable('SsoClientSessions_Create',
			'SELECT * FROM SsoClientSessions ORDER BY datecreated DESC')
				->getRow(0);

		$this->assertNotEmpty($newRow['datecreated']);
		unset($newRow['datecreated']);
		$this->assertNotEmpty($newRow['datechecked']);
		unset($newRow['datechecked']);

		$this->assertEquals(
			array(
				 'sessionid' => $session,
				 'activated' => 1,
				 'clientidentifier' => $this->clientCode
			),
			$newRow
		);

		$this->assertEquals($oldCount + 1, $this->getConnection()->getRowCount('SsoClientSessions'));
	}

	public function testDeleteSession() {
		$this->assertFalse($this->object->deleteSession('sessiontest1'));
		$this->assertTrue($this->object->deleteSession('sessiontest2'));
		$this->assertTrue($this->object->deleteSession('sessiontest3'));
		$this->assertTrue($this->object->deleteSession('sessiontest4'));
		$this->assertTrue($this->object->deleteSession('sessiontest5'));

		$this->assertEquals(1, $this->getConnection()->getRowCount('SsoClientSessions'));
	}

	public function testCleanUp() {
		$this->object->cleanUp();

		$this->assertEquals(3, $this->getConnection()->getRowCount('SsoClientSessions'));
	}

    /**
     * Real-life stress-test
     *
     * Try to activate and update a session quite shortly after it's created
     * Some SQL engines do not return the count of found data, but effectively updated data. This leads to strange
     * behaviour here, since we early counted the updated rows. But the only data that changed was the date in seconds.
     * But if the updating of a session was executed less than a second after it's updating, this failed.
     */
    public function testCreateActivateAndUpdateSession() {
        $session = $this->object->createSession();
        $this->assertTrue($this->object->activateSession($session));
        $this->assertTrue($this->object->updateSession($session));
    }
}
