<?php

/**
 * Memcached itself is only available for linux.
 */
class SherwoodSignOn_SessionRepository_MemcachedTest extends PHPUnit_Framework_TestCase {

	/**
	 * @var SherwoodSignOn_SessionRepository_Memcached
	 */
	protected $object;

	/**
	 * @var Memcached
	 */
	protected $memcache;

	/**
	 * @var string
	 */
	private $clientCode = "UnitTest";

	/**
	 * @var string
	 */
	private $config;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		if (!class_exists("Memcached"))
			$this->markTestSkipped("Memcached is not installed as PHP-extension");

		$this->memcache = new Memcached;
		$this->memcache->addServer($GLOBALS['MEMCACHE_HOST'], $GLOBALS['MEMCACHE_PORT']);

		$this->object = new SherwoodSignOn_SessionRepository_Memcached($this->memcache);
		$this->object->setConfiguration(5, $this->clientCode);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		if ($this->memcache) {
			$this->memcache->flush();
			sleep(1);
		}
	}

	public function testSetConfiguration() {
		$reflection_class = new ReflectionClass("SherwoodSignOn_SessionRepository_Memcached");
		$propertySessionTimeoutInMinutes = $reflection_class->getProperty('sessionTimeoutInMinutes');
		$propertySessionTimeoutInMinutes->setAccessible(true);

		$propertyClientIdentifier = $reflection_class->getProperty('clientIdentifier');
		$propertyClientIdentifier->setAccessible(true);

		$propertyInstance = $reflection_class->getProperty('instance');
		$propertyInstance->setAccessible(true);

		$instance = new SherwoodSignOn_SessionRepository_Memcached();

		$this->assertEquals(null, $propertySessionTimeoutInMinutes->getValue($instance));
		$this->assertEquals(null, $propertyClientIdentifier->getValue($instance));
		$this->assertEquals(null, $propertyInstance->getValue($instance));

		$instance->setConfiguration(5, $this->clientCode, array(
            'host' => $GLOBALS['MEMCACHE_HOST'],
            'port' => $GLOBALS['MEMCACHE_PORT']
        ));

		$this->assertEquals("5", $propertySessionTimeoutInMinutes->getValue($instance));
		$this->assertEquals($this->clientCode, $propertyClientIdentifier->getValue($instance));
		$this->assertEquals('Memcached', get_class($propertyInstance->getValue($instance)));
	}

	public function testCreateSession() {
		$sessionId = $this->object->createSession();

		$session = $this->memcache->get(SherwoodSignOn_SessionRepository_Interface::uniqueKey . '|' . $this->clientCode . "|" . $sessionId);
		$this->assertEquals("0", $session);
	}

	public function testActivateSession() {
		$sessionId = $this->object->createSession();

		// Do I get TRUE if the session is acitvated?
		$result = $this->object->activateSession($sessionId);
		$this->assertEquals(true, $result);

		// Is the session really activated?
		$session = $this->memcache->get(SherwoodSignOn_SessionRepository_Interface::uniqueKey . '|' . $this->clientCode . "|" . $sessionId);
		$this->assertEquals("1", $session);

		// What about an non-existing or expired session-id?
		$result = $this->object->activateSession($sessionId . "2");
		$this->assertEquals(false, $result);
	}

	public function testUpdateSession() {
		$sessionId = $this->object->createSession();

		// You should not be able to update a non-activated session.
		$result = $this->object->updateSession($sessionId);
		$this->assertEquals(false, $result);

		$this->object->activateSession($sessionId);

		$reflection_class = new ReflectionClass("SherwoodSignOn_SessionRepository_Memcached");
		$property = $reflection_class->getProperty('sessionTimeoutInMinutes');
		$property->setAccessible(true);

		// Update Session with new time (1 sec)
		$property->setValue($this->object, 1 / 60);
		$result = $this->object->updateSession($sessionId);
		$this->assertEquals(true, $result);

		// Reading the session should work.
		$session = $this->memcache->get(SherwoodSignOn_SessionRepository_Interface::uniqueKey . '|' . $this->clientCode . "|" . $sessionId);
		$this->assertEquals("1", $session);
		sleep(1);
		// After one sec. the session should be expired.
		$session = $this->memcache->get(SherwoodSignOn_SessionRepository_Interface::uniqueKey . '|' . $this->clientCode . "|" . $sessionId);
		$this->assertEquals(false, $session);
	}

	public function testDeleteSession() {
		$sessionId = $this->object->createSession();
		$this->object->deleteSession($sessionId);

		$session = $this->memcache->get(SherwoodSignOn_SessionRepository_Interface::uniqueKey . '|' . $this->clientCode . "|" . $sessionId);
		$this->assertEquals(false, $session);
	}

	public function testCleanUp() {
		// Nothing to test
	}

    /**
     * Real-life stress-test
     *
     * Try to activate and update a session quite shortly after it's created
     * Some SQL engines do not return the count of found data, but effectively updated data. This leads to strange
     * behaviour here, since we early counted the updated rows. But the only data that changed was the date in seconds.
     * But if the updating of a session was executed less than a second after it's updating, this failed.
     */
    public function testCreateActivateAndUpdateSession() {
        $session = $this->object->createSession();
        $this->assertTrue($this->object->activateSession($session));
        $this->assertTrue($this->object->updateSession($session));
    }
}
