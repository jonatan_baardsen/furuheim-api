<?php

class SherwoodSignOn_FirewallTest extends PHPUnit_Framework_TestCase {

	public function testHasUserAccess() {
		$minimalConfigDefaultDeny = simplexml_load_string('<accessRules><defaultDeny>true</defaultDeny><allowed></allowed>
			<denied></denied></accessRules>');

		$minimalConfigDefaultAllow = simplexml_load_string('<accessRules><defaultDeny>false</defaultDeny><allowed></allowed>
			<denied></denied></accessRules>');

		$testConfig = simplexml_load_string('<accessRules><defaultDeny>true</defaultDeny>
			<allowed><role>test:Group1</role><role>test:Group2</role><user>User1</user></allowed>
			<denied><role>test:Group3</role><user>User2</user><user>User3</user></denied></accessRules>');

		$userToDenyByGroup = simplexml_load_string('<getUserProfileResponse>
			<userAccountId></userAccountId>
			<username>testuser</username>
			<firstName></firstName>
			<lastName></lastName>
			<country></country>
			<dateOfBirth></dateOfBirth>
			<gender>0</gender>
			<email></email>
			<globalUser>false</globalUser>
			<role name="unregistered_group" source="test" />
			<role name="Group3" source="test" />
		</getUserProfileResponse>');

		$userToDenyByUsername = simplexml_load_string('<getUserProfileResponse>
				<userAccountId></userAccountId>
				<username>User2</username>
				<firstName></firstName>
				<lastName></lastName>
				<country></country>
				<dateOfBirth></dateOfBirth>
				<gender>0</gender>
				<email></email>
				<globalUser>false</globalUser>
				<role name="unregistered_group" source="test" />
			</getUserProfileResponse>');

		$userToAllowByGroup = simplexml_load_string('<getUserProfileResponse>
				<userAccountId></userAccountId>
				<username>testuser</username>
				<firstName></firstName>
				<lastName></lastName>
				<country></country>
				<dateOfBirth></dateOfBirth>
				<gender>0</gender>
				<email></email>
				<globalUser>false</globalUser>
				<role name="Group1" source="test" />
			</getUserProfileResponse>');

		$userToAllowByUsernameCaseSensitive = simplexml_load_string('<getUserProfileResponse>
				<userAccountId></userAccountId>
				<username>User1</username>
				<firstName></firstName>
				<lastName></lastName>
				<country></country>
				<dateOfBirth></dateOfBirth>
				<gender>0</gender>
				<email></email>
				<globalUser>false</globalUser>
				<role name="unregistered_group" source="test" />
			</getUserProfileResponse>');

		$userToAllowByUsernameCaseInsensitive = simplexml_load_string('<getUserProfileResponse>
				<userAccountId></userAccountId>
				<username>UsEr1</username>
				<firstName></firstName>
				<lastName></lastName>
				<country></country>
				<dateOfBirth></dateOfBirth>
				<gender>0</gender>
				<email></email>
				<globalUser>false</globalUser>
				<role name="unregistered_group" source="test" />
			</getUserProfileResponse>');

		$userToAllowByGroupButDenyUsername = simplexml_load_string('<getUserProfileResponse>
				<userAccountId></userAccountId>
				<username>User2</username>
				<firstName></firstName>
				<lastName></lastName>
				<country></country>
				<dateOfBirth></dateOfBirth>
				<gender>0</gender>
				<email></email>
				<globalUser>false</globalUser>
				<role name="Group2" source="test" />
			</getUserProfileResponse>');

		$userToDenyByGroupButAllowByUsername = simplexml_load_string('<getUserProfileResponse>
				<userAccountId></userAccountId>
				<username>User1</username>
				<firstName></firstName>
				<lastName></lastName>
				<country></country>
				<dateOfBirth></dateOfBirth>
				<gender>0</gender>
				<email></email>
				<globalUser>false</globalUser>
				<role name="Group3" source="test" />
			</getUserProfileResponse>');

		$testUser = new SherwoodSignOn_UserProfile($userToDenyByGroup);

		$object = new SherwoodSignOn_Firewall(new SherwoodSignOn_Configuration_Firewall_Xml($minimalConfigDefaultAllow));
		$this->assertTrue($object->hasUserAccess($testUser));

        $object = new SherwoodSignOn_Firewall(new SherwoodSignOn_Configuration_Firewall_Xml($minimalConfigDefaultDeny));
		$this->assertFalse($object->hasUserAccess($testUser));

        $object = new SherwoodSignOn_Firewall(new SherwoodSignOn_Configuration_Firewall_Xml($testConfig));

		$user = new SherwoodSignOn_UserProfile($userToDenyByGroup);
		$this->assertFalse($object->hasUserAccess($user));

		$user = new SherwoodSignOn_UserProfile($userToDenyByUsername);
		$this->assertFalse($object->hasUserAccess($user));

		$user = new SherwoodSignOn_UserProfile($userToAllowByGroup);
		$this->assertTrue($object->hasUserAccess($user));

		$user = new SherwoodSignOn_UserProfile($userToAllowByUsernameCaseSensitive);
		$this->assertTrue($object->hasUserAccess($user));

		$user = new SherwoodSignOn_UserProfile($userToAllowByUsernameCaseInsensitive);
		$this->assertTrue($object->hasUserAccess($user));

		$user = new SherwoodSignOn_UserProfile($userToAllowByGroupButDenyUsername);
		$this->assertFalse($object->hasUserAccess($user));

		$user = new SherwoodSignOn_UserProfile($userToDenyByGroupButAllowByUsername);
		$this->assertTrue($object->hasUserAccess($user));
	}

	public function testRedirectToDenyUrl() {
		// Nothing that could be covered by a unit-test.
	}
}
