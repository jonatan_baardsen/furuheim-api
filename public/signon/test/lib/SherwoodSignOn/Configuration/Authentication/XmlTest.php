<?php

class XmlTest extends PHPUnit_Framework_TestCase {

    /**
     * @var SherwoodSignOn_Configuration_Xml
     */
    private $defaultConfig;

    protected function setUp() {
        $this->defaultConfig = new SherwoodSignOn_Configuration_Xml(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/data/SSOClientSettings.xml");
    }

    public function testGetClientCode() {
        $this->assertEquals("soo_test_code", $this->defaultConfig->getClientCode());
    }

    public function testGetClientRsaPrivateKey() {
        $this->assertEquals("-----BEGIN RSA PRIVATE KEY-----

-----END RSA PRIVATE KEY-----", $this->defaultConfig->getClientRsaPrivateKey());
    }

    public function testGetServerRsaPublicKey() {
        $this->assertEquals("-----BEGIN PUBLIC KEY-----
MIGdMA0GCSqGSIb3DQEBAQUAA4GLADCBhwKBgQDtM2ZRCnrru0XCaaaIJNWmdmGa
obCnMxqGrymDel7nJW6H8TfWbnh9linYvje62b8/tLPdyVJShrshtxKOQKha141F
MKNCqb6pqSwnOhn5mv/83QvurbvCJTl/q/CKUKdU2mVsyskA4o1WygrLUjwxYLKX
EDO8xuM9SRhwoFfO/QIBAw==
-----END PUBLIC KEY-----", $this->defaultConfig->getServerRsaPublicKey());
    }

    public function testServerBaseUrl() {
        $this->assertEquals("https://signon.brunstad.org/", $this->defaultConfig->getServerBaseUrl());
    }
}
