<?php

class SherwoodSignOn_Configuration_XmlTest extends PHPUnit_Framework_TestCase {

    /**
     * @var SherwoodSignOn_Configuration_Xml
     */
    private $defaultConfig;

    protected function setUp() {
        $this->defaultConfig = new SherwoodSignOn_Configuration_Xml(dirname(dirname(dirname(dirname(__FILE__)))) . "/data/SSOClientSettings.xml");
    }

    public function testGetClientSignInUrl() {
        $this->assertEquals("http://[Host]/signon/signin.php?ticket={signinticket}&signature={signinsignature}&returnurl=[UrlEncodedCurrentUrl]",
            $this->defaultConfig->getClientSignInUrl());
    }

    public function testGetClientSignOutUrl() {
        $this->assertEquals("http://[Host]/signon/signout.php?ticket={signoutticket}&signature={signoutsignature}",
            $this->defaultConfig->getClientSignOutUrl());
    }

}
