<?php

/**
 * This implementation should only be used by experienced developers!
 * Here you can authenticate an user by his username and password.
 *
 * You should have read and understood this articles before you continue:
 * http://www.sslshopper.com/article-how-to-make-a-secure-login-form-with-ssl.html
 * http://www.owasp.org/index.php/SSL_Best_Practices
 *
 * Do !never! submit a form, containing sensitive data like username and password using GET. An url incl. it's
 * parameters is never encrypted, even so the connection is done using HTTPS, and will be logged by the provider, any
 * proxy-server in between and in your server's log.
 */

require_once(dirname(__FILE__) . "/lib/SherwoodSignOn/Autoloader.php");
SherwoodSignOn_Autoloader::getInstance()->register();

$username = "MyUsername";
$password = "MyPassword";

$SignOnClient = new SherwoodSignOn_Client();

try {
	$SignOnProfile = $SignOnClient->authenticateUser($username, $password);
} catch(SherwoodSignOn_Exception_RemoteServiceEmptyResult $e) {
	/*
	 * Don't ask me why, but the current Sherwood-Server implementation just returns an empty result when the username
	 * or password was incorrect.
	 */
	header("HTTP/1.1 403 Forbidden");
	die("403 Forbidden");
}

if ($SignOnClient->getClientSettings()->hasFirewall())
    $firewall = new SherwoodSignOn_Firewall($SignOnClient->getClientSettings()->getFirewall());
    if (!$firewall->hasUserAccess($SignOnProfile)) {
        /*
         * Just DIE in this example... If you implement this somewhere, I recommend to return the same value as if the
         * user has been found, but the credentials, like the password didn't match (if your system supports this state)
         * or just that the user could not be authenticated.
         */
        header("HTTP/1.1 403 Forbidden");
        die("403 Forbidden");
    }

print "<pre>";
print_r($SignOnProfile);
print "</pre>";
