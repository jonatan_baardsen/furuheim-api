<?php

require_once(dirname(__FILE__) . "/lib/SherwoodSignOn/Autoloader.php");
SherwoodSignOn_Autoloader::getInstance()->register();

$client = new SherwoodSignOn_Client();

$ticket = isset($_GET['ticket']) ? $_GET['ticket'] : "";
$signature = isset($_GET['signature']) ? $_GET['signature'] : "";

$client->clientSignOut($ticket, $signature);
