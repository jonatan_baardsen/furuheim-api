<style type="text/css">
	table { border-collapse: collapse; }

	table td { padding: .2em; }
</style>
<?php
function outputInstalledVersion($module, $expected = "", $isOptional = false) {
	if ($module !== null && !extension_loaded($module))
		return "<span style='color: red'>Not installed</span>";

	if ($module === null)
		$installedVersion = phpversion();
	else
		$installedVersion = phpversion($module);

	if ($installedVersion === false)
		return "<span style='color: green'>Installed</span>";
	elseif (strnatcmp($installedVersion, $expected) >= 0)
		return "<span style='color: green'>{$installedVersion}</span>";
	elseif ($isOptional)
		return "<span style='color: green'>{$installedVersion}</span>";
	else
		return "<span style='color: yellowgreen;'>{$installedVersion}</span>";
}

function getAtomicTime() {
  $fp = fsockopen("time.nist.gov", 37, $errno, $errstr, 30);
  if ($fp) {
    $data = "";
    while (!feof($fp)) {
      $data .= fgets($fp, 128);
    }
    fclose($fp);

    // we have a response...is it valid? (4 char string -> 32 bits)
    if (strlen($data) == 4) {
      // time server response is a string - convert to numeric
      $NTPtime = ord($data{0})*pow(256, 3) + ord($data{1})*pow(256, 2) + ord($data{2})*256 + ord($data{3});
      // convert the seconds to the present date & time
      $linuxTime = $NTPtime - 0x83AA7E80;
    
      return new DateTime('@'.$linuxTime);
    }
  }

  return null;
}

?>
<table border="1">
	<tr>
		<td>Name</td>
		<td>Required</td>
		<td>Current</td>
	</tr>
	<tr>
		<td>PHP</td>
		<td>5.2.2</td>
		<td><?php echo outputInstalledVersion(null, '5.2.2'); ?></td>
	</tr>
	<tr>
		<td>OpenSSL</td>
		<td>Installed</td>
		<td><?php echo outputInstalledVersion('openssl'); ?></td>
	</tr>
	<tr>
		<td>cUrl</td>
		<td>Installed</td>
		<td><?php echo outputInstalledVersion('curl'); ?></td>
	</tr>
	<tr>
		<td>SimpleXML</td>
		<td>Installed</td>
		<td><?php echo outputInstalledVersion('simplexml'); ?></td>
	</tr>
	<tr>
		<td>APC</td>
		<td>3.0.0 *</td>
		<td><?php echo outputInstalledVersion('apc', '3.0.0', true); ?></td>
	</tr>
	<tr>
		<td>MySQL</td>
		<td>Installed *</td>
		<td><?php echo outputInstalledVersion('mysql', '', true); ?></td>
	</tr>
	<tr>
		<td>MySQLi</td>
		<td>Installed *</td>
		<td><?php echo outputInstalledVersion('mysqli', '', true); ?></td>
	</tr>
	<tr>
		<td>Memcache</td>
		<td>0.2.0 *</td>
		<td><?php echo outputInstalledVersion('memcache', '0.2.0', true); ?></td>
	</tr>
	<tr>
		<td>Memcached</td>
		<td>0.1.0 *</td>
		<td><?php echo outputInstalledVersion('memcached', '0.1.0', true); ?></td>
	</tr>
	<tr>
		<td>PDO</td>
		<td>Installed *</td>
		<td>
			<?php echo outputInstalledVersion('pdo', '', true); ?>
			<?php if (extension_loaded('pdo')) : ?>
			<?php foreach (PDO::getAvailableDrivers() as $driver) {
				echo "<br/>&nbsp;&nbsp;" . $driver;
			} ?>
			<?php endif; ?>
		</td>
	</tr>
  <tr>
    <td>Default Timezone</td>
    <td>configured</td>
    <td>
      <?php $timezone = ini_get('date.timezone'); ?>
      <?php if (empty($timezone)) : ?>
        <?php echo "<span style='color: red'>not defined</span>"; ?>
      <?php elseif (!in_array($timezone, DateTimeZone::listIdentifiers())) : ?>
        <?php echo "<span style='color: red'>\"$timezone\" is invalid</span>"; ?>
      <?php else : ?>
        <?php echo "<span style='color: green'>$timezone</span>"; ?>
      <?php endif; ?>
    </td>
  </tr>
  <tr>
    <td>Difference between<br/>server and atomic time</td>
    <td>&lt; 300 sec</td>
    <td>
      <?php $atomicTime = getAtomicTime(); ?>
      <?php $now = new DateTime('now', new DateTimeZone('UTC') ); ?>
      <?php if (empty($atomicTime)) : ?>
        <?php echo "<span style='color: yellowgreen'>Current Server time: ".$now->format("r")."</span><br/>Atomic server is not responding."; ?>
      <?php else :
        $diff = $atomicTime->format("U") - $now->format("U");
        if ($diff < 0) $diff = $diff*-1;
        if ($diff > 300) : ?>
          <?php echo "<span style='color: red'>$diff sec</span>"; ?>
        <?php else : ?>
          <?php echo "<span style='color: green'>$diff sec</span>"; ?>
      <?php endif; endif; ?>
    </td>
  </tr>
</table>

<p>* At least one of this plugins has to be installed.</p>
