<?php

/**
 * Class for storing session values
 */
class SherwoodSignOn_UserProfileRepository_Session implements SherwoodSignOn_UserProfileRepository_Interface {

	/**
	 * @param $clientIdentifier
	 * @return void
	 */
	public function setClientIdentifier($clientIdentifier) {
	}

	/**
	 * @return boolean
	 * @codeCoverageIgnore
	 */
	protected function startSession() {
		if (session_id() === "") {
			// If the session is not started until now
			if (headers_sent())
				return false;

			session_start();
		}

		return true;
	}

	/**
	 * @param SherwoodSignOn_UserProfile $userProfile
	 * @return boolean
	 */
	public function setUserProfile(SherwoodSignOn_UserProfile $userProfile) {
		if ($this->startSession()) {
			$_SESSION[self::uniqueKey] = array('profile' => serialize($userProfile), 'saveTime' => time());
			return true;
		}
		return false;
	}

	/**
	 * @param string $userProfileId
	 * @return boolean|SherwoodSignOn_UserProfile
	 */
	public function getUserProfile($userProfileId) {
		if ($this->startSession() && isset($_SESSION[self::uniqueKey])) {
			$profileData = $_SESSION[self::uniqueKey];
			if (!is_array($profileData) || $profileData['saveTime'] + self::maxCachingTime < time())
				$this->deleteUserProfile($userProfileId);
			else
				return @unserialize($profileData['profile']);
		}

		return false;
	}

	/**
	 * @param string $userProfileId
	 * @return boolean
	 */
	public function deleteUserProfile($userProfileId) {
		if ($this->startSession()) {
			unset($_SESSION[self::uniqueKey]);
			return true;
		}
		return false;
	}
}
