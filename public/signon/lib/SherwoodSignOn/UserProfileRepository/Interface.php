<?php

interface SherwoodSignOn_UserProfileRepository_Interface {

	const maxCachingTime = 1800; // halv an hour
	const uniqueKey = 'BrunstadSignon_UserProfile'; // Unique prefix for shared repositories

	/**
	 * @param $clientIdentifier
	 * @return void
	 */
	public function setClientIdentifier($clientIdentifier);

	/**
	 * @param SherwoodSignOn_UserProfile $userProfile
	 * @return boolean
	 */
	public function setUserProfile(SherwoodSignOn_UserProfile $userProfile);

	/**
	 * @param string $userProfileId
	 * @return boolean|SherwoodSignOn_UserProfile
	 */
	public function getUserProfile($userProfileId);

	/**
	 * @param string $userProfileId
	 * @return boolean
	 */
	public function deleteUserProfile($userProfileId);
}
