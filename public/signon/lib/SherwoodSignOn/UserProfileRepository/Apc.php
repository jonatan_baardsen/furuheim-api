<?php

/**
 * Class for storing session values
 */
class SherwoodSignOn_UserProfileRepository_Apc implements SherwoodSignOn_UserProfileRepository_Interface {

	private $clientIdentifier;

	/**
	 * @param $clientIdentifier
	 * @return void
	 */
	public function setClientIdentifier($clientIdentifier) {
		$this->clientIdentifier = $clientIdentifier;
	}

	/**
	 * @param SherwoodSignOn_UserProfile $userProfile
	 * @return boolean
	 */
	public function setUserProfile(SherwoodSignOn_UserProfile $userProfile) {
		return apc_store(self::uniqueKey . '|' . $this->clientIdentifier . '|' . $userProfile->id, $userProfile,
			self::maxCachingTime);
	}

	/**
	 * @param string $userProfileId
	 * @return boolean|SherwoodSignOn_UserProfile
	 */
	public function getUserProfile($userProfileId) {
		return apc_fetch(self::uniqueKey . '|' . $this->clientIdentifier . '|' . $userProfileId);
	}

	/**
	 * @param string $userProfileId
	 * @return boolean
	 */
	public function deleteUserProfile($userProfileId) {
		apc_delete(self::uniqueKey . '|' . $this->clientIdentifier . '|' . $userProfileId);
		return true;
	}
}
