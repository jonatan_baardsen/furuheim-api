<?php

interface SherwoodSignOn_Configuration_Interface extends SherwoodSignOn_Configuration_Authentication_Interface {

    /**
     * Path on your implementation to signin.php or a PHP file, handling the same event.
     * The access to this path must bypass this library!
     *
     * @return string
     */
    public function getClientSignInUrl();

    /**
     * Path on your implementation to signout.php or a PHP file, handling the same event.
     * The access to this path must bypass this library!
     *
     * @return string
     */
    public function getClientSignOutUrl();

    /**
     * Returns the time in seconds, for how long a server-ticket is considered valid. This kind of ticket is the time,
     * how long the client can remain at the server's side.
     * After this library discovers, that the client isn't logged in, he's redirected to the SSO-Server. Depending on
     * whether the user is already logged in there, he either get back to your page quite soon or is holt back there for
     * a while.
     *
     * @return int
     */
    public function getServerTicketValidity();

    /**
     * Name of the class (may be in the namespace of SherwoodSignOn_SessionRepository_*) controlling the
     * SessionRepository. This class must implement SherwoodSignOn_SessionRepository_Interface.
     *
     * @return string
     */
    public function getSessionRepositoryType();

    /**
     * Returns an associative array of settings.
     *
     * @return array
     */
    public function getSessionRepositorySettings();

    /**
     * Returns the time in minutes, for how long an entry in the sessionRepository is considered valid.
     * The sessionRepository keeps the information, that the user is logged in at the server, so you don't need to
     * redirect him to the server again to figure that out.
     *
     * @return int
     */
    public function getSessionRepositoryTimeout();

    /**
     * @return bool
     */
    public function hasFirewall();

    /**
     * Depending on if the configuration has a configuration for the firewall, it returns either the instance or null.
     *
     * @return SherwoodSignOn_Configuration_Firewall_Interface|null
     */
    public function getFirewall();

}