<?php

class SherwoodSignOn_Configuration_Firewall_Xml implements SherwoodSignOn_Configuration_Firewall_Interface {

    private $isDenyByDefault;
    private $accessDeniedUrl;

    private $allowedUsernames = array();
    private $deniedUsernames = array();

    private $allowedRoles = array();
    private $deniedRoles = array();

    /**
     * @param SimpleXMLElement $xmlHandler
     */
	public function __construct(SimpleXMLElement $xmlHandler) {
        $this->isDenyByDefault = strtolower($xmlHandler->defaultDeny) === "true";
        $this->accessDeniedUrl = $xmlHandler->denyRedirectUrl;

        foreach($xmlHandler->allowed->children() as $child) {
            /** @var $child SimpleXMLElement */
            if ($child->getName() === "user")
                $this->allowedUsernames[] = strtolower((string)$child);
            else if ($child->getName() === "role")
                $this->allowedRoles[] = (string)$child;
        }

        foreach($xmlHandler->denied->children() as $child) {
            if ($child->getName() === "user")
                $this->deniedUsernames[] = strtolower((string)$child);
            else if ($child->getName() === "role")
                $this->deniedRoles[] = (string)$child;
        }
	}

    /**
     * @inheritdoc
     */
    public function isDefaultDeny()
    {
        return $this->isDenyByDefault;
    }

    /**
     * @inheritdoc
     */
    public function getAccessDeniedUrl()
    {
        return $this->accessDeniedUrl;
    }

    /**
     * @inheritdoc
     */
    public function getAllowedByUsername()
    {
        return $this->allowedUsernames;
    }

    /**
     * @inheritdoc
     */
    public function getAllowedByRoles()
    {
        return $this->allowedRoles;
    }

    /**
     * @inheritdoc
     */
    public function getDeniedByUsername()
    {
        return $this->deniedUsernames;
    }

    /**
     * @inheritdoc
     */
    public function getDeniedByRoles()
    {
        return $this->deniedRoles;
    }
}
