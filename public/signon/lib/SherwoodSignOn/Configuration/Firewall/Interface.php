<?php

/**
 * Class SherwoodSignOn_Configuration_Firewall_Interface
 *
 * Attention! Usernames must be converted to lowercase! The server handles them case-insensitive and we just get it in
 * lowercase in the library.
 */
interface SherwoodSignOn_Configuration_Firewall_Interface {

    /**
     * @return bool
     */
    public function isDefaultDeny();

    /**
     * @return string
     */
    public function getAccessDeniedUrl();

    /**
     * @return string[]
     */
    public function getAllowedByUsername();

    /**
     * @return string[]
     */
    public function getAllowedByRoles();

    /**
     * @return string[]
     */
    public function getDeniedByUsername();

    /**
     * @return string[]
     */
    public function getDeniedByRoles();
}