<?php

class SherwoodSignOn_Configuration_Xml extends SherwoodSignOn_Configuration_Authentication_Xml implements SherwoodSignOn_Configuration_Interface {

    /**
     * @inheritdoc
     */
    public function getClientSignInUrl()
    {
        return (string)$this->xmlHandler->client->signInUrl;
    }

    /**
     * @inheritdoc
     */
    public function getClientSignOutUrl()
    {
        return (string)$this->xmlHandler->client->signOutUrl;
    }

    /**
     * @inheritdoc
     */
    public function getServerTicketValidity()
    {
        return (int)$this->xmlHandler->server->ticketValidityInSeconds;
    }

    /**
     * @inheritdoc
     */
    public function getSessionRepositoryType()
    {
        return (string)$this->xmlHandler->sessionRepository->connection->type;
    }

    /**
     * @inheritdoc
     */
    public function getSessionRepositorySettings()
    {
        $array = array();
        foreach($this->xmlHandler->sessionRepository->connection->children() as $k => $v) {
            $array[$k] = (string)$v;
        }
        unset($array['type']);

        return $array;
    }

    /**
     * @inheritdoc
     */
    public function getSessionRepositoryTimeout()
    {
        return (int)$this->xmlHandler->sessionRepository->sessionTimeoutInMinutes;
    }

    /**
     * @inheritdoc
     */
    public function hasFirewall()
    {
        return $this->xmlHandler->accessRules !== NULL;
    }

    /**
     * @inheritdoc
     */
    public function getFirewall()
    {
        return new SherwoodSignOn_Configuration_Firewall_Xml($this->xmlHandler->accessRules);
    }
}
