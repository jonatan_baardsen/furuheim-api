<?php

interface SherwoodSignOn_Configuration_Authentication_Interface {

    /**
     * @return string
     */
    public function getClientCode();

    /**
     * @return string
     */
    public function getClientRsaPrivateKey();

    /**
     * @return bool
     */
    public function isClientTimeDiffCheckEnabled();

    /**
     * @return string
     */
    public function getServerBaseUrl();

    /**
     * @return string
     */
    public function getServerRsaPublicKey();
}