<?php

class SherwoodSignOn_Configuration_Authentication_Xml implements SherwoodSignOn_Configuration_Authentication_Interface {

	/**
	 * @var SimpleXMLElement
	 */
	protected $xmlHandler;

	/**
	 * @param string $XMLFilename The name of the xml file.
	 * @throws Exception
	 */
	public function __construct($XMLFilename = '') {

		if (empty($XMLFilename))
			$XMLFilename = 'SSOClientSettings.xml';

        // Check if the argument contains a path or just a filename
		if (strpos($XMLFilename, DIRECTORY_SEPARATOR) === false) {
			$path = dirname($_SERVER['DOCUMENT_ROOT']) . DIRECTORY_SEPARATOR . $XMLFilename;
		} else {
			$path = $XMLFilename;
		}

		//check if file is readable
		if (!is_readable($path)) {
			throw new Exception("The configuration-file is not readable. Check your reference: " . $path);
		}

		$this->xmlHandler = simplexml_load_file($path);
	}

    /**
     * @inheritdoc
     */
    public function getClientCode()
    {
        return (string)$this->xmlHandler->client->code;
    }

    /**
     * @inheritdoc
     */
    public function getClientRsaPrivateKey()
    {
        return (string)$this->xmlHandler->client->rsaPrivateKey;
    }

    /**
     * @inheritdoc
     */
    public function isClientTimeDiffCheckEnabled()
    {
        return strtolower((string)$this->xmlHandler->client->disableTimeDiffCheck) === "true";
    }

    /**
     * @inheritdoc
     */
    public function getServerRsaPublicKey()
    {
        return (string)$this->xmlHandler->server->rsaPublicKey;
    }

    /**
     * @inheritdoc
     */
    public function getServerBaseUrl()
    {
        return (string)$this->xmlHandler->server->baseUrl;
    }
}
