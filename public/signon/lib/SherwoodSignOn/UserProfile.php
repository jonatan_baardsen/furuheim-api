<?php

class SherwoodSignOn_UserProfile {

	/** @var string users gid */
	public $id = '00000000-0000-0000-0000-000000000000';

	/** @var string */
	public $username = 'anonymous';

	/** @var string */
	public $firstName = 'Anonymous';

	/** @var string */
	public $lastName = 'User';

	/**  @var string[] */
	public $cultures = array();

	/**  @var string[] */
	public $roles = array();

	/**  @var string[] */
	public $attributes = array();

	/**
	 * @var string ISO 3166-1-alpha-2 code
	 * @look http://www.iso.org/iso/english_country_names_and_code_elements
	 */
	public $country = '';

	/**  @var string The birthdate as string. yyyy-mm-dd */
	public $dateOfBirth = '';

	/** @var string */
	public $email = '';

	/** @var string */
	public $mobile = '';

	/**
	 * @var int
	 *		 0 - unknown
	 *		 1 - male
	 *		 2 - female
	 */
	public $gender = 0;

	/** @var boolean */
	public $globalUser = true;

	/**
	 * Creates a new uer profile.
	 *
	 * @param SimpleXMLElement $simpleXmlObject
	 */
	public function __construct(SimpleXMLElement $simpleXmlObject = null) {
		if (!empty($simpleXmlObject)) {
			$this->id = (string)$simpleXmlObject->userAccountId;
			$this->username = (string)$simpleXmlObject->username;
			$this->firstName = (string)$simpleXmlObject->firstName;
			$this->lastName = (string)$simpleXmlObject->lastName;
			$this->country = (string)$simpleXmlObject->country;
			$this->dateOfBirth = (string)$simpleXmlObject->dateOfBirth;
			$this->gender = (int)$simpleXmlObject->gender;
			$this->email = (string)$simpleXmlObject->email;
			$this->mobile = (string)$simpleXmlObject->mobile;
			$this->globalUser = (bool)$simpleXmlObject->globalUser;

			$this->cultures = $this->getCultures($simpleXmlObject);
			$this->attributes = $this->getAttributes($simpleXmlObject);
			$this->roles = $this->getRoles($simpleXmlObject);
		}
	}

	/**
	 * @param SimpleXMLElement $simpleXmlObject
	 * @return array
	 */
	private function getCultures($simpleXmlObject) {
		$cultures = array();
		$cultureXML = $simpleXmlObject->culture;

		if ($cultureXML !== null) {
			foreach ($cultureXML as $culture)
				/** @var $culture SimpleXMLElement */
				$cultures[] = (string)$culture->attributes()->name;
		}

		return $cultures;
	}

	/**
	 * @param SimpleXMLElement $simpleXmlObject
	 * @return array
	 */
	private function getAttributes($simpleXmlObject) {
		$attributes = array();
		$attributeXML = $simpleXmlObject->attribute;

		if ($attributeXML !== null) {
			foreach ($attributeXML as $attribute) {
				/** @var $attribute SimpleXMLElement */
				$key = (string)$attribute->attributes()->source . ":" . (string)$attribute->attributes()->name;
				$attributes[$key] = (string)$attribute->attributes()->value;
			}
		}

		return $attributes;
	}

	/**
	 * @param SimpleXMLElement $simpleXmlObject
	 * @return array
	 */
	private function getRoles($simpleXmlObject) {
		$roles = array();
		$roleXML = $simpleXmlObject->role;

		if ($roleXML !== null) {
			foreach ($roleXML as $role)
				/** @var $role SimpleXMLElement */
				$roles[] = (string)$role->attributes()->source . ":" . (string)$role->attributes()->name;
		}

		return $roles;
	}
}
