<?php

class SherwoodSignOn_Autoloader {

	/**
	 * @var SherwoodSignOn_Autoloader
	 */
	protected static $instance = null;

	protected $prefixes = array();


	protected function __construct() {
		$this->prefixes['SherwoodSignOn'] = array(dirname(dirname(__FILE__)));
	}

	/**
	 * @codeCoverageIgnore
	 */
	private function __clone() {
	}

	/**
	 * @static
	 * @return SherwoodSignOn_Autoloader
	 */
	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	/**
	 * @param string $className
	 * @return string|boolean Returns the path to the class or false if the class has not been registered
	 */
	protected function getClassPath($className) {
		// PEAR-like class name
		foreach ($this->prefixes as $prefix => $dirs) {
			foreach ($dirs as $dir) {
				if (0 === strpos($className, $prefix)) {
					$file = $dir . DIRECTORY_SEPARATOR . str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
					if (file_exists($file)) {
						return $file;
					}
				}
			}
		}
		return FALSE;
	}

	/**
	 * @param string $className
	 * @return boolean Return true if the class can be loaded or is still loaded, or returns false if the class is not registered. Maybe this class is loaded by another autoloader.
	 * @codeCoverageIgnore
	 */
	public function loadClass($className) {
		$path = $this->getClassPath($className);

		if ($path === FALSE) {
			return FALSE;
		} else {
			include_once $path;
			return TRUE;
		}
	}

	/**
	 * Regsiter the autoloader
	 * @return void
	 */
	public function register() {
		spl_autoload_register(array($this, 'loadClass'));
	}
}
