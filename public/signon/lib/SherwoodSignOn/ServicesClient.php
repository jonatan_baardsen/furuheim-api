<?php

class SherwoodSignOn_ServicesClient {

	private $baseUrl;
	private $clientCode;
	private $rsaPrivateKey;
    private $skipTimeDiffToServer;

	/**
	 * @param string $baseUrl
	 * @param string $clientCode
	 * @param string $rsaPrivateKey
     * @param boolean $skipTimeDiffToServer
	 */
	public function __construct($baseUrl, $clientCode, $rsaPrivateKey, $skipTimeDiffToServer = true) {
		$this->baseUrl = $baseUrl;
		$this->clientCode = $clientCode;
        $this->rsaPrivateKey = $rsaPrivateKey;
        $this->skipTimeDiffToServer = $skipTimeDiffToServer;
	}

	/**
	 * @return SherwoodSignOn_Server
	 * @codeCoverageIgnore
	 */
	protected function getServer() {
		return new SherwoodSignOn_Server($this->baseUrl);
	}

	/**
	 * Returns the profile of the user associated with the serverSessionId.
	 *
	 * @param string $serverSessionId
	 * @throws SherwoodSignOn_Exception_InvalidSignature
	 * @throws SherwoodSignOn_Exception_RemoteServiceEmptyResult
	 * @throws SherwoodSignOn_Exception_RemoteServiceException
	 * @return SherwoodSignOn_UserProfile
	 */
	public function getUserProfile($serverSessionId) {

		//Assemble request for user profile to Sso service
		$clientCode = urlencode($this->clientCode);

		// Generate an esitmated server-time out of the current local time and the server-time-diff
		$serverTime = gmdate("Y-m-d\TH:i:s.111\Z", time() - $this->getTimeDiffToServer());

		$signatureData = $clientCode . "|" . $serverTime . "|" . $serverSessionId;
		$signature = $this->signData($signatureData);

		$result = $this->getServer()->getUserProfile($clientCode, $serverTime, $serverSessionId, $signature);

		return $this->parseServerResultToProfile($result);
	}

	/**
	 * Parses the result of the server that is either an exception (which will be thrown) or an user-profile
	 *
	 * @param string $result
	 * @throws SherwoodSignOn_Exception_InvalidSignature
	 * @throws SherwoodSignOn_Exception_RemoteServiceEmptyResult
	 * @throws SherwoodSignOn_Exception_RemoteServiceException
	 * @return SherwoodSignOn_UserProfile
	 */
	protected function parseServerResultToProfile($result) {
		//Perform request to Sso GetUserProfile service
		$result = str_replace(' xmlns="Sherwood.SignOn"', '', $result);
		$result = str_replace(' xmlns="Sherwood"', '', $result);

		// If the respond is empty. Maybe you are an anonymous user - but (in anyway) you've passed the anonymous-test
		if ($result === '')
			throw new SherwoodSignOn_Exception_RemoteServiceEmptyResult();

		$xml = simplexml_load_string($result);

		// If an exception occured on the server side script
		if ($xml->getName() === "serviceException")
			throw new SherwoodSignOn_Exception_RemoteServiceException($xml);

		return new SherwoodSignOn_UserProfile($xml);
	}

	/**
	 * Returns the profile of the user associated with the username and password.
	 *
	 * @param string $username
	 * @param string $password
	 * @throws SherwoodSignOn_Exception_InvalidSignature
	 * @throws SherwoodSignOn_Exception_RemoteServiceEmptyResult
	 * @throws SherwoodSignOn_Exception_RemoteServiceException
	 * @return SherwoodSignOn_UserProfile
	 */
	public function authenticate($username, $password) {

		//Assemble request for user profile to Sso service
		$clientCode = urlencode($this->clientCode);

		// Generate an esitmated server-time out of the current local time and the server-time-diff
        $serverTime = gmdate("Y-m-d\TH:i:s.111\Z", time() + $this->getTimeDiffToServer());

		$signatureData = $clientCode . "|" . $serverTime . "|" . $username . "|" . $password;
		$signature = $this->signData($signatureData);

		$result = $this->getServer()->authenticate($clientCode, $serverTime, $username, $password, $signature);

		return $this->parseServerResultToProfile($result);
	}

	/**
	 * Creates a signature by hashing the message using the SHA1 algorithm and encrypting the hash with a private key.
	 * Message is converted to an array of UTF-8 encoded bytes before being hashed.
	 *
	 * @param string $message Message to be signed
	 * @throws SherwoodSignOn_Exception_InvalidSignature
	 * @return string
	 */
	protected function signData($message) {
		$key = openssl_pkey_get_private($this->rsaPrivateKey);

		if (!is_resource($key))
			throw new SherwoodSignOn_Exception_InvalidSignature("Client's private ssl-key is invalid");

		openssl_sign($message, $signature, $key);
		openssl_free_key($key);

		return base64_encode($signature);
	}

	/**
	 * Load a timestamp from the server and check how much time our server is behind the signon-server (including one
	 * request and one response)
	 *
	 * @return int
	 */
	public function getTimeDiffToServer() {

        if ($this->skipTimeDiffToServer)
            return 0;

        $serverTime = $this->getServer()->getTime();
        $clientTime = new DateTime();

		return (int)$serverTime->format("U") - $clientTime->format("U");
	}
}
