<?php

class SherwoodSignOn_Server {

	private $baseUrl;

	/**
	 * @param string $baseUrl
	 */
	public function __construct($baseUrl) {
		$this->baseUrl = trim($baseUrl, "/");
	}

	/**
	 * @param string $clientSessionId
	 * @param string $localSignInUrl
	 * @param string $localSignOutUrl
	 * @param boolean $requireLogin
	 * @return string
	 */
	public function getLoginUrl($clientSessionId, $localSignInUrl, $localSignOutUrl, $requireLogin) {
		$params = array(
			'ClientSessionId' => $clientSessionId,
			'SignOutUrl' => $localSignOutUrl,
			'ReturnUrl' => $localSignInUrl
		);

		if (!$requireLogin)
			$params['RequireLogin'] = "False";

		return $this->baseUrl . "/SignIn/?" . http_build_query($params);
	}

	/**
	 * @return DateTime
	 * @throws Exception
	 */
	public function getTime() {
		$serverTime = $this->getFromRemote($this->baseUrl . "/Services/ping");

		try {
			$time = new DateTime($serverTime);
		} catch (Exception $e) {
			throw new Exception("The time given by the remote Server is invalid.", 0, $e);
		}

		return $time;
	}

	/**
	 * @param string $clientCode
	 * @param string $timestamp
	 * @param string $serverSessionId
	 * @param string $signature
	 * @return string
	 */
	public function getUserProfile($clientCode, $timestamp, $serverSessionId, $signature) {
		$params = array(
			'ClientCode' => $clientCode,
			'Timestamp' => $timestamp,
			'SignOnSessionId' => $serverSessionId,
			'Signature' => $signature
		);

		$requestUrl = $this->baseUrl . "/Services/GetSessionProfile?" . http_build_query($params);
		return $this->getFromRemote($requestUrl);
	}

	/**
	 * @param string $clientCode
	 * @param string $timestamp
	 * @param string $username
	 * @param string $password
	 * @param string $signature
	 * @return string
	 */
	public function authenticate($clientCode, $timestamp, $username, $password, $signature) {
		$params = array(
			'ClientCode' => $clientCode,
			'Timestamp' => $timestamp,
			'Username' => $username,
			'Password' => $password,
			'Signature' => $signature
		);

		$requestUrl = $this->baseUrl . "/Services/Authenticate?" . http_build_query($params);
		return $this->getFromRemote($requestUrl);
	}

	/**
	 * @param string $requestUrl
	 * @return string
	 * @throws ErrorException
	 * @codeCoverageIgnore
	 */
	protected function getFromRemote($requestUrl) {
		$ch = curl_init($requestUrl);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);

		if ($response === false) {
			$errorno = curl_errno($ch);
			$errormsg = curl_error($ch);
			curl_close($ch);
			throw new ErrorException('curl-error (' . $errorno . '): ' . $errormsg);
		}
		curl_close($ch);

		return $response;
	}
}
