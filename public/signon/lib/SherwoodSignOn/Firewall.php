<?php

class SherwoodSignOn_Firewall {

    const ALLOW = 1;
    const DENY = -1;
    const UNHANDLED = 0;

	/** @var SherwoodSignOn_Configuration_Firewall_Interface */
	private $_config = null;

	/**
	 * @param SherwoodSignOn_Configuration_Firewall_Interface $config
	 */
	public function __construct(SherwoodSignOn_Configuration_Firewall_Interface $config) {
		$this->_config = $config;
	}

	/**
	 * @return int
	 */
	protected function hasAccessByDefault() {
		return !$this->_config->isDefaultDeny() ? self::ALLOW : self::DENY;
	}

	/**
	 * @param SherwoodSignOn_UserProfile $profile
	 * @return int
	 */
	protected function hasAccessByRole(SherwoodSignOn_UserProfile $profile) {
		foreach ($this->_config->getAllowedByRoles() as $role) {
			if (in_array($role, $profile->roles))
				return self::ALLOW;
		}

		foreach ($this->_config->getDeniedByRoles() as $role) {
			if (in_array($role, $profile->roles))
				return self::DENY;
		}

		return self::UNHANDLED;
	}

	/**
	 * @param SherwoodSignOn_UserProfile $profile
	 * @return int
	 */
	protected function hasAccessByName(SherwoodSignOn_UserProfile $profile) {
        if (in_array(strtolower($profile->username), $this->_config->getAllowedByUsername()))
            return self::ALLOW;

        if (in_array(strtolower($profile->username), $this->_config->getDeniedByUsername()))
            return self::DENY;

		return self::UNHANDLED;
	}

	/**
	 * @param SherwoodSignOn_UserProfile $profile
	 * @return boolean
	 */
	public function hasUserAccess(SherwoodSignOn_UserProfile $profile) {
		if (($access = $this->hasAccessByName($profile)) === self::UNHANDLED) {
			if (($access = $this->hasAccessByRole($profile)) === self::UNHANDLED) {
				$access = $this->hasAccessByDefault();
			}
		}

		if ($access === self::ALLOW)
			return true;
		else
			return false;
	}

	/**
	 * @codeCoverageIgnore
	 */
	public function redirectToDenyUrl() {
		$url = $this->_config->getAccessDeniedUrl();

		if (empty($url)) {
			header("Status: 403 Forbidden");
			echo <<<HTML
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<title>403 Access Denied</title>
	</head>
	<body>
		<h1>403 Access Denied</h1>
		<p>You are not authorized to view this page.</p>
	</body>
</html>
HTML;
		} else {
			header("Status: 307 Temporary Redirect");
			header("Cache-Control: no-cache");
			header("Location: $url");
		}

		die();
	}
}
