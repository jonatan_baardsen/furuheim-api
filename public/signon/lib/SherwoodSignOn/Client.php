<?php

/**
 * Class for the sign on client
 */
class SherwoodSignOn_Client {

	/** @var boolean */
	private $_requireLogin;

	/** @var string|void */
	private $_userProfileId = null;

	/** @var SherwoodSignOn_SessionRepository_Interface|void */
	private $_sessionRepository = null;

	/** @var SherwoodSignOn_ClientsideSessionRepository_Interface|void */
	private $_clientsideSessionRepository = null;

	/** @var SherwoodSignOn_UserProfileRepository_Interface|void */
	private $_userProfileRepository = null;

	/** @var SherwoodSignOn_Configuration_Authentication_Interface */
	protected $_config = null;

	/** @var boolean */
	private $_isAnonymousUser = false;

	/** @var string */
	private $_serverSessionId = '';

	/** @var string|void */
	private $_clientSessionId = null;

	/** @var boolean */
	public $debug = false;

	/**
	 * @param boolean $requireLogin
	 */
	public function __construct($requireLogin = true) {
		$this->_requireLogin = (boolean)$requireLogin;
	}


	/**
	 * Set an instance of a persistent user-profile repository
	 *
	 * @param SherwoodSignOn_UserProfileRepository_Interface $value
	 * @return void
	 */
	public function setUserProfileRepository(SherwoodSignOn_UserProfileRepository_Interface $value) {
		$value->setClientIdentifier($this->getClientSettings()->getClientCode());
		$this->_userProfileRepository = $value;
	}

	/**
	 * Returns an instance of a persistent user-profile repository
	 * which can be used to prevent the script requesting the
	 * profile from the server at every call
	 *
	 * @return SherwoodSignOn_UserProfileRepository_Interface
	 */
	public function getUserProfileRepository() {
		if ($this->_userProfileRepository === null)
			$this->setUserProfileRepository(new SherwoodSignOn_UserProfileRepository_Session());

		return $this->_userProfileRepository;
	}

	/**
	 * Set an instance of a persistent session repository
	 *
	 * @param SherwoodSignOn_SessionRepository_Interface $value
	 * @param boolean $updateSettings If this function should update the settins in your SessionRepository
	 * @return void
	 */
	public function setSessionRepository(SherwoodSignOn_SessionRepository_Interface $value, $updateSettings = true) {
		$this->_sessionRepository = $value;

		if ($updateSettings) {
			$config = $this->getClientSettings();
			$this->_sessionRepository->setConfiguration(
                $config->getSessionRepositoryTimeout(),
                $config->getClientCode(),
                $config->getSessionRepositorySettings()
			);
		}
	}

	/**
	 * Returns an instance of a persistent session repository which
	 * can be used to maintain records of the client's active sessions.
	 *
	 * @throws InvalidArgumentException
	 * @return SherwoodSignOn_SessionRepository_Interface
	 */
	public function getSessionRepository() {
		if ($this->_sessionRepository === null) {
			$className = (string)$this->getClientSettings()->getSessionRepositoryType();
			if (class_exists('SherwoodSignOn_SessionRepository_' . $className))
				$className = 'SherwoodSignOn_SessionRepository_' . $className;
			if (!class_exists($className))
				throw new InvalidArgumentException("The value '{$className}' is not a valid value for the sessionRepository type in your configuration. Please check the documentation or send an email to signon-support@brunstad.org");
			$value = new $className;

			$this->setSessionRepository($value);
		}

		return $this->_sessionRepository;
	}

	/**
	 * Set an instance of a clientside session repository
	 * Meant for Cookies and PHP-Session (as may differ from framework to framework)
	 *
	 * @param SherwoodSignOn_ClientsideSessionRepository_Interface $value
	 * @return void
	 */
	public function setClientsideSessionRepository(SherwoodSignOn_ClientsideSessionRepository_Interface $value) {
		$this->_clientsideSessionRepository = $value;
	}

	/**
	 * Returns an instance of a persistent session repository which
	 * can be used to maintain records of the client's active sessions.
	 *
	 * @throws InvalidArgumentException
	 * @return SherwoodSignOn_ClientsideSessionRepository_Interface
	 */
	public function getClientsideSessionRepository() {
		if ($this->_clientsideSessionRepository === null) {
			$value = new SherwoodSignOn_ClientsideSessionRepository_Cookie($this->getClientSettings()->getClientCode());
			$this->setClientsideSessionRepository($value);
		}

		return $this->_clientsideSessionRepository;
	}

	/**
	 * @param SherwoodSignOn_Configuration_Authentication_Interface $configuration
	 */
	public function setClientSettings(SherwoodSignOn_Configuration_Authentication_Interface $configuration) {
		$this->_config = $configuration;
	}

	/**
	 * @return SherwoodSignOn_Configuration_Authentication_Interface|SherwoodSignOn_Configuration_Interface
	 */
	public function getClientSettings() {
		if ($this->_config === null)
			$this->_config = new SherwoodSignOn_Configuration_Xml();

		return $this->_config;
	}


	/**
	 * Sets SessionId, IsAnonymousUser and SignOnSessionId item variables if a session exists
	 * otherwise, if no session exists, these variables are cleared.
	 *
	 * @return void
	 */
	private function checkSession() {
		// If this check wasn't executed before ...
		if ($this->_clientSessionId === null) {

			// Init variables
			$this->_clientSessionId = null;
			$this->_serverSessionId = null;
			$this->_isAnonymousUser = false;

			// Fetch the session
			$parts = $this->getClientsideSessionRepository()->getSessionData();

			// Verify, that the session is in a valid format (has 3 parts). It also verifies if a session exists at all.
			if (count($parts) !== 3)
				return;

			// Put all parts into their server-variables
			$this->_clientSessionId = $parts[0];
			$this->_serverSessionId = $parts[1];
			$this->_userProfileId = $parts[2];

			// Check if the user is an anonymous user (uuid for null)
			if ($this->_userProfileId === '00000000-0000-0000-0000-000000000000')
				$this->_isAnonymousUser = true;

			// If session can't be updated, kill it and reset all variables.
			if (!$this->getSessionRepository()->updateSession($this->_clientSessionId)) {
				$this->localClientSignOut();

				$this->_clientSessionId = null;
				$this->_serverSessionId = null;
				$this->_isAnonymousUser = false;
			}
		}
	}

	/**
	 * Returns the current (local) session id or null if no session exists.
	 * Internally sets _userProfileId
	 *
	 * @return string|void
	 */
	public function getClientSessionId() {
		$this->checkSession();
		return $this->_clientSessionId;
	}

	/**
	 * Returns the SessionId provided by the sign on server (global session Id)
	 *
	 * @return string
	 */
	public function getServerSessionId() {
		$this->checkSession();
		return $this->_serverSessionId;
	}

	/**
	 * Indicates whether or not the user is signed in.
	 *
	 * @return boolean
	 */
	public function isUserSignedIn() {
		if ($this->getClientSessionId())
			return true;
		else
			return false;
	}

	/**
	 * Returns the UserProfile object for this user.
	 * This functionality is only applicable to "trusted clients"
	 * for which the SSO Server has a record of their RSA public key.
	 *
     * @throws SherwoodSignOn_Exception_InvalidSignature
     * @throws SherwoodSignOn_Exception_RemoteServiceEmptyResult
     * @throws SherwoodSignOn_Exception_RemoteServiceException
	 * @return SherwoodSignOn_UserProfile
	 */
	public function getUserProfile() {

		$serverSessionId = $this->getServerSessionId();

		// If the user is an anonymous user - repeat it immediately
		if (empty($serverSessionId) || $this->_isAnonymousUser)
			return new SherwoodSignOn_UserProfile();

		$profile = $this->getUserProfileRepository()->getUserProfile($this->_userProfileId);
		if ($profile && $profile instanceof SherwoodSignOn_UserProfile) {
			return $profile;
		} else {
			$config = $this->getClientSettings();
			$servicesClient = new SherwoodSignOn_ServicesClient(
                $config->getServerBaseUrl(),
                $config->getClientCode(),
                $config->getClientRsaPrivateKey(),
                $config->isClientTimeDiffCheckEnabled()
            );

			try {
				$profile = $servicesClient->getUserProfile($serverSessionId);
			} catch (SherwoodSignOn_Exception_RemoteServiceException $e) {
				// If the server cannot find the given session, just clean it up and try to log in again.
				if ($e->getMessage() === 'SignOnSession not found in database.') {
					$this->redirectToSsoServer();
				} else
					throw $e;
			}

			$this->getUserProfileRepository()->setUserProfile($profile);

			return $profile;
		}
	}

    /**
     * Authenticates the user and returns the UserProfile object for this user.
     * This functionality is only applicable to "trusted clients"
     * for which the SSO Server has a record of their RSA public key.
     *
     * @param $username
     * @param $password
     * @throws SherwoodSignOn_Exception_InvalidSignature
     * @throws SherwoodSignOn_Exception_RemoteServiceEmptyResult
     * @throws SherwoodSignOn_Exception_RemoteServiceException
     * @return SherwoodSignOn_UserProfile
     */
	public function authenticateUser($username, $password) {

		$config = $this->getClientSettings();
		$servicesClient = new SherwoodSignOn_ServicesClient(
            $config->getServerBaseUrl(),
            $config->getClientCode(),
            $config->getClientRsaPrivateKey()
        );

		$profile = $servicesClient->authenticate($username, $password);

		return $profile;
	}

	/**
	 * Registers a local session and redirects the user to the SSO server
	 *
	 * @return void
	 */
	public function redirectToSsoServer() {

		// The current session must be deleted before redirecting to the sso-server
		$clientSessionId = $this->getClientSessionId();
		if ($clientSessionId !== null) {
			$this->localClientSignOut($clientSessionId);
		}

		$newClientSessionId = $this->getSessionRepository()->createSession();
		// Automatic cleanup of old sessions
		if (mt_rand(0, 10) == 5) {
			$this->getSessionRepository()->cleanUp();
		}

		$server = new SherwoodSignOn_Server($this->getClientSettings()->getServerBaseUrl());
		$signInUrl = $server->getLoginUrl(
			$newClientSessionId,
			$this->getSignInUrl(),
			$this->getSignOutUrl(),
			$this->_requireLogin
		);

		if ($this->debug) {
			echo "Status: 302 Found\n";
			echo "Location: $signInUrl\n";
			echo "Cache-Control: no-cache\n";
		} else {
			header("Status: 302 Found");
			header("Location: $signInUrl");
			header("Cache-Control: no-cache");
			exit();
		}
	}

	/**
	 * @param string $ticket
	 * @param string $signature
	 * @param string $suggestedPrefix
	 * @throws SherwoodSignOn_Exception_TicketExpired
	 * @throws SherwoodSignOn_Exception_InvalidSignature
	 * @throws InvalidArgumentException
	 * @return array The full description of the return-value: array($prefix, $serverSessionId,
	 *                          $clientSessionId, $ipAddress, $userProfileId, $timestampAsString, $isPersistentSession)
	 */
	private function validateTicket($ticket, $signature, $suggestedPrefix) {
		if (empty($ticket) || empty($signature))
			throw new InvalidArgumentException("Ticket or signature is missing");

		//Decode ticket (expected to be base64 encoded string of UTF8 encoded bytes)
		$ticket = base64_decode($ticket);

        $key = openssl_pkey_get_public($this->getClientSettings()->getServerRsaPublicKey());

		if (!is_resource($key))
            throw new SherwoodSignOn_Exception_InvalidSignature("Server's public ssl-key is invalid");

        $isVerified = openssl_verify($ticket, base64_decode($signature), $key);
        openssl_free_key($key);

		if ($isVerified !== 1)
			throw new InvalidArgumentException("Signature mismatch");

		// Parse ticket contents
		$ticketParts = explode('|', $ticket);
		if (count($ticketParts) < 6)
			throw new InvalidArgumentException("Ticket does not have expected count of parameters");

		$prefix = $ticketParts[0];
		$serverSessionId = $ticketParts[1];
		$clientSessionId = $ticketParts[2];
		$timestampString = $ticketParts[5];

		// Validate ticket contents
		if (strtoupper($prefix) !== $suggestedPrefix)
			throw new InvalidArgumentException("Ticket has an invalid prefix");

		if (empty($serverSessionId))
			throw new InvalidArgumentException("Ticket is missing a server-session-id");

		if (empty($clientSessionId))
			throw new InvalidArgumentException("Ticket is missing a client-session-id");

		$localTime = new DateTime();
		$remoteTime = new DateTime($timestampString);
		$diff = $localTime->format("U") - $remoteTime->format("U");
		if ($diff > (int)$this->getClientSettings()->getServerTicketValidity())
			throw new SherwoodSignOn_Exception_TicketExpired();

		return $ticketParts;
	}

	/**
	 * Performs local sign-in of the user.
	 *
	 * @param string $ticket Ticket from SSO server (base64, utf8 encoded string)
	 * @param string $signature Ticket signature
	 * @throws SherwoodSignOn_Exception_TicketExpired
	 * @throws SherwoodSignOn_Exception_InvalidSignature
	 * @throws InvalidArgumentException
	 * @return boolean True if sign on was successful, otherwise false
	 */
	public function clientSignOn($ticket, $signature) {

		list(, $serverSessionId, $clientSessionId, , $userProfileId,) = $this->validateTicket($ticket, $signature,
		    "SI");

		if ($this->getSessionRepository()->activateSession($clientSessionId)) {
			$this->getClientsideSessionRepository()->setSessionData(
				array($clientSessionId, $serverSessionId, $userProfileId)
			);
			return true;
		} else {
			if ($this->debug)
				echo "Client Session could not be not found in the SessionRepository.";
			return false;
		}
	}

	/**
	 * Performs a signout by a ticket-id and its signature.
	 * This function is designed to be only triggered by the sso-server.
	 *
	 * @param string $ticket Ticket from SSO server (base64, utf8 encoded string)
	 * @param string $signature Ticket signature
	 * @throws SherwoodSignOn_Exception_TicketExpired
	 * @throws SherwoodSignOn_Exception_InvalidSignature
	 * @throws InvalidArgumentException
	 * @return boolean True if sign out was successful, otherwise false
	 */
	public function clientSignOut($ticket, $signature) {

		list(, , $clientSessionId, , $userProfileId,) = $this->validateTicket($ticket, $signature, "SO");

		$this->localClientSignOut($clientSessionId, $userProfileId);
		return true;
	}

	/**
	 * Performs a local sign-out by the user-sso-session.
	 * The session is removed from the client- and local-serverside session repository.
	 *
	 * @param string $clientSessionId
	 * @param string $userProfileId
	 * @return void
	 */
	public function localClientSignOut($clientSessionId = '', $userProfileId = '') {
		if ($clientSessionId === '')
			$clientSessionId = $this->getClientSessionId();

		if ($userProfileId === '')
			$userProfileId = $this->_userProfileId;

		$this->getSessionRepository()->deleteSession($clientSessionId);
		$this->getUserProfileRepository()->deleteUserProfile($userProfileId);
		$this->getClientsideSessionRepository()->removeSessionData();
	}

	/**
	 * Determines current, fully qualified domain name.
	 * Relies on $_SERVER['HTTP_HOST'] being set.
	 *
	 * @return string The current (host) domain, or the sample-domain example.com if no host value was passed.
	 */
	protected function getCurrentHost() {
		// An empty host (provided by some bots) could create some errors. Just return the sample-domain.
		if (empty($_SERVER['HTTP_HOST'])) {
			return "example.com";
		} else {
			// Otherwise, trim and return the HTTP_HOST value for checking against registered domains. Note that we
			// lower case this, since the RFC states that EXAMPLE.com == example.com.
			return strtolower(rtrim($_SERVER['HTTP_HOST']));
		}
	}

	/**
	 * @return string
	 */
	protected function getProtocol() {
		$protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https' ? 'https' : 'http';
		return $protocol;
	}

	/**
	 * @return string
	 */
	protected function getCurrentUrl() {
		$host = $this->getProtocol() . "://" . $this->getCurrentHost();

		if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443")
			$host .= ":" . $_SERVER["SERVER_PORT"];

		return $host . $_SERVER["REQUEST_URI"];
	}

	/**
	 * @return string
	 */
	protected function getSignInUrl() {
		return str_replace(array('[Host]', '[UrlEncodedCurrentUrl]', '[Protocol]'),
			array($this->getCurrentHost(), urlencode($this->getCurrentUrl()), $this->getProtocol()),
			$this->getClientSettings()->getClientSignInUrl());
	}

	/**
	 * @return string
	 */
	protected function getSignOutUrl() {
		return str_replace('[Host]', $this->getCurrentHost(), $this->getClientSettings()->getClientSignOutUrl());
	}
}
