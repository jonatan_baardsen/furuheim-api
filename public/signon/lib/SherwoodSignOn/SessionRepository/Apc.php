<?php

class SherwoodSignOn_SessionRepository_Apc implements SherwoodSignOn_SessionRepository_Interface {

	protected $clientIdentifier;
	protected $sessionTimeoutInMinutes;

	/**
     * @inheritdoc
	 */
	public function setConfiguration($sessionTimeoutInMinutes, $clientIdentifier, array $configuration = array()) {
		$this->clientIdentifier = $clientIdentifier;
		$this->sessionTimeoutInMinutes = $sessionTimeoutInMinutes;
	}

	/**
	 * Registers a new (unactivated) session and returns the session identifier
	 *
	 * @return string
	 */
	public function createSession() {
		$sessionId = uniqid(time());

		// one hour for filling out the login-form should be enough ...
		apc_store(self::uniqueKey . '|' . $this->clientIdentifier . '|' . $sessionId, 0, 3600);

		return $sessionId;
	}

	/**
	 * Validates the session identifier and activates the session.
	 * A session identifier is not valid in this context if the session has already been activated.
	 * This function is called after creating the session. It only activates the session if it's not
	 *
	 * @param string $sessionId Client session identifier
	 * @return boolean True if identifier could be validated and session already exists, otherwise false.
	 */
	public function activateSession($sessionId) {
		if (apc_fetch(self::uniqueKey . '|' . $this->clientIdentifier . '|' . $sessionId) === 0) {
			apc_store(self::uniqueKey . '|' . $this->clientIdentifier . '|' . $sessionId, 1,
					$this->sessionTimeoutInMinutes * 60);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Update Session
	 * This function is called every time the user requests this webservice
	 *
	 * @param string $sessionId
	 * @return boolean
	 */
	public function updateSession($sessionId) {
		if (apc_fetch(self::uniqueKey . '|' . $this->clientIdentifier . '|' . $sessionId) === 1) {
			apc_store(self::uniqueKey . '|' . $this->clientIdentifier . '|' . $sessionId, 1,
					$this->sessionTimeoutInMinutes * 60);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Remove session from repository. This method should be called as part of the single sign off process.
	 *
	 * @param string $sessionId
	 * @return boolean
	 */
	public function deleteSession($sessionId) {
		return apc_delete(self::uniqueKey . '|' . $this->clientIdentifier . '|' . $sessionId);
	}

	/**
	 * We don't need a cleanup-function because memcached will overwrite one of the expired entries as soon as a new
	 * session will be opened or it is requested after expiration.
	 * Just implement this function to implement the interface correctly.
	 *
	 * @return void
	 * @codeCoverageIgnore
	 */
	public function cleanUp() {
	}
}
