<?php

class SherwoodSignOn_SessionRepository_Pdo implements SherwoodSignOn_SessionRepository_Interface {

	private $_dsn;
	private $_username;
	private $_password;

	protected $clientIdentifier;
	protected $sessionTimeoutInMinutes;
	protected $pdo;

    /**
     * @inheritdoc
     */
    public function setConfiguration($sessionTimeoutInMinutes, $clientIdentifier, array $configuration = array()) {
        $this->clientIdentifier = $clientIdentifier;

        $this->_dsn = $configuration['dsn'];
        $this->_username = $configuration['username'];
        $this->_password = $configuration['password'];

        $this->sessionTimeoutInMinutes = $sessionTimeoutInMinutes;
	}

	/**
	 * @return string
	 */
	protected function getExpiredDate() {
		$expiredDate = new DateTime();
		$expiredDate->modify("-{$this->sessionTimeoutInMinutes} minutes");
		return $expiredDate->format('Y-m-d H:i:s');
	}

	/**
	 * @return PDO
	 */
	private function getPdoInstance() {
		if (!$this->pdo) {
			$this->pdo = new PDO($this->_dsn, $this->_username, $this->_password);
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
		}

		return $this->pdo;
	}

	/**
	 * Registers a new (unactivated) session and returns the session identifier
	 *
	 * @return string
	 */
	public function createSession() {
		$sessionId = uniqid(time());
		$sql = "INSERT INTO SsoClientSessions (sessionid,activated,clientidentifier,datecreated) VALUES (:sessionId, 0, :clientIdentifier, :now)";

		$now = date('Y-m-d H:i:s');
		$sth = $this->getPdoInstance()->prepare($sql);
		$sth->bindParam(':sessionId', $sessionId, PDO::PARAM_STR);
		$sth->bindParam(':clientIdentifier', $this->clientIdentifier, PDO::PARAM_STR);
		$sth->bindParam(':now', $now, PDO::PARAM_STR);
		$sth->execute();

		return $sessionId;
	}

	/**
	 * Validates the session identifier and activates the session.
	 * A session identifier is not valid in this context if the session has already been activated.
	 * This function is called after creating the session. It only activates the session if it's not
	 *
	 * @param string $sessionId Client session identifier
	 * @return boolean True if identifier could be validated and session already exists, otherwise false.
	 */
	public function activateSession($sessionId) {
		$sql = "UPDATE SsoClientSessions SET datechecked = :now, activated = 1 WHERE
				sessionid = :sessionId
				AND clientidentifier = :clientIdentifier
				AND activated = 0
				AND datecreated > :expiredDate
		";

		$now = date('Y-m-d H:i:s');
		$expiredDate = $this->getExpiredDate();
		$sth = $this->getPdoInstance()->prepare($sql);
		$sth->bindParam(':sessionId', $sessionId, PDO::PARAM_STR);
		$sth->bindParam(':expiredDate', $expiredDate, PDO::PARAM_STR);
		$sth->bindParam(':clientIdentifier', $this->clientIdentifier, PDO::PARAM_STR);
		$sth->bindParam(':now', $now, PDO::PARAM_STR);
		$sth->execute();

		return $sth->rowCount() > 0;
	}

	/**
	 * Update Session
	 * This function is called every time the user requests this webservice
	 *
	 * @param string $sessionId
	 * @return boolean
	 */
	public function updateSession($sessionId) {
		$sqlCount = "SELECT COUNT(*) FROM SsoClientSessions WHERE
				sessionid = :sessionId
				AND clientidentifier = :clientIdentifier
				AND activated = 1
				AND datechecked > :expiredDate
		";

        $sqlUpdate = "UPDATE SsoClientSessions SET datechecked = :now WHERE
				sessionid = :sessionId
				AND clientidentifier = :clientIdentifier
				AND activated = 1
				AND datechecked > :expiredDate
		";

		$expiredDate = $this->getExpiredDate();

		$sthCount = $this->getPdoInstance()->prepare($sqlCount);
        $sthCount->bindParam(':sessionId', $sessionId, PDO::PARAM_STR);
        $sthCount->bindParam(':expiredDate', $expiredDate, PDO::PARAM_STR);
        $sthCount->bindParam(':clientIdentifier', $this->clientIdentifier, PDO::PARAM_STR);
        $sthCount->execute();

		$now = date('Y-m-d H:i:s');
		$expiredDate = $this->getExpiredDate();
		$sth = $this->getPdoInstance()->prepare($sqlUpdate);
		$sth->bindParam(':sessionId', $sessionId, PDO::PARAM_STR);
		$sth->bindParam(':expiredDate', $expiredDate, PDO::PARAM_STR);
		$sth->bindParam(':clientIdentifier', $this->clientIdentifier, PDO::PARAM_STR);
		$sth->bindParam(':now', $now, PDO::PARAM_STR);
		$sth->execute();

		return $sthCount->fetchColumn() > 0;
	}

	/**
	 * Remove session from repository. This method should be called as part of the single sign off process.
	 *
	 * @param string $sessionId
	 * @return boolean
	 */
	public function deleteSession($sessionId) {
		$sql = "DELETE FROM SsoClientSessions WHERE sessionid = :sessionId AND clientidentifier = :clientIdentifier";

		$sth = $this->getPdoInstance()->prepare($sql);
		$sth->bindParam(':sessionId', $sessionId, PDO::PARAM_STR);
		$sth->bindParam(':clientIdentifier', $this->clientIdentifier, PDO::PARAM_STR);
		$sth->execute();

		return $sth->rowCount() > 0;
	}

	/**
	 * Clean up any expired sessions. This could for example be called on application_start.
	 *
	 * @return void
	 */
	public function cleanUp() {
		$sql = "DELETE FROM SsoClientSessions WHERE
			clientidentifier = :clientIdentifier
			AND (
				(
					activated = 0
					AND datecreated < :expiredDate
				) OR (
					activated = 1
					AND datechecked < :expiredDate
				)
			)";

		$expiredDate = $this->getExpiredDate();
		$sth = $this->getPdoInstance()->prepare($sql);
		$sth->bindParam(':clientIdentifier', $this->clientIdentifier, PDO::PARAM_STR);
		$sth->bindParam(':expiredDate', $expiredDate, PDO::PARAM_STR);
		$sth->execute();
	}
}
