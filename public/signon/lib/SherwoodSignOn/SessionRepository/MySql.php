<?php

/**
 * @deprecated
 */
class SherwoodSignOn_SessionRepository_MySql implements SherwoodSignOn_SessionRepository_Interface {

	private $_host;
	private $_database;
	private $_username;
	private $_password;

	protected $clientIdentifier;
	protected $sessionTimeoutInMinutes;

    /**
     * @inheritdoc
     */
    public function setConfiguration($sessionTimeoutInMinutes, $clientIdentifier, array $configuration = array()) {
        $this->clientIdentifier = $clientIdentifier;

        $this->_host = $configuration['host'];
        $this->_database = $configuration['database'];
        $this->_username = $configuration['username'];
        $this->_password = $configuration['password'];

        $this->sessionTimeoutInMinutes = $sessionTimeoutInMinutes;
    }

	/**
	 * @return resource
	 */
	private function getServerConnection() {
		if (!($link = mysql_connect($this->_host, $this->_username, $this->_password)))
			die("Connect failed: " . mysql_error());

		if (!mysql_select_db($this->_database, $link))
			die("Connect failed: " . mysql_error($link));

		return $link;
	}

	/**
	 * @return string
	 */
	protected function getExpiredDate() {
		$expiredDate = new DateTime();
		$expiredDate->modify("-{$this->sessionTimeoutInMinutes} minute");
		return $expiredDate->format('Y-m-d H:i:s');
	}

	/**
	 * Registers a new (unactivated) session and returns the session identifier
	 *
	 * @return string
	 */
	public function createSession() {
		$sessionId = uniqid(time());
		$link = $this->getServerConnection();
		$sql = "INSERT INTO SsoClientSessions (sessionid,activated,clientidentifier,datecreated) VALUES
			('$sessionId', 0, '" . mysql_real_escape_string($this->clientIdentifier, $link) . "', NOW())";

		if (!mysql_query($sql, $link))
			die("Could not execute SQL statement: $sql because " . mysql_error($link));

		mysql_close($link);

		return $sessionId;
	}

	/**
	 * Validates the session identifier and activates the session.
	 * A session identifier is not valid in this context if the session has already been activated.
	 * This function is called after creating the session. It only activates the session if it's not
	 *
	 * @param string $sessionId Client session identifier
	 * @return boolean True if identifier could be validated and session already exists, otherwise false.
	 */
	public function activateSession($sessionId) {
		$link = $this->getServerConnection();
		$sql = "UPDATE SsoClientSessions SET datechecked = NOW(), activated = TRUE WHERE
				sessionid = '" . mysql_real_escape_string($sessionId, $link) . "'
				AND clientidentifier = '" . mysql_real_escape_string($this->clientIdentifier, $link) . "'
				AND activated = FALSE
				AND datecreated > '" . $this->getExpiredDate() . "'
		";

		if (!mysql_query($sql, $link))
			die("Could not execute SQL statement: $sql because " . mysql_error($link));
		$count = mysql_affected_rows($link);

		mysql_close($link);

		return $count > 0;
	}

	/**
	 * Update Session
	 * This function is called every time the user requests this webservice
	 *
	 * @param string $sessionId
	 * @return boolean
	 */
	public function updateSession($sessionId) {
		$link = $this->getServerConnection();
		$sql = "SELECT COUNT(*) FROM SsoClientSessions WHERE
				sessionid = '" . mysql_real_escape_string($sessionId, $link) . "'
				AND clientidentifier = '" . mysql_real_escape_string($this->clientIdentifier, $link) . "'
				AND activated = TRUE
				AND datechecked > '" . $this->getExpiredDate() . "'
		";

		if (($res = mysql_query($sql, $link)) === FALSE)
			die("Could not execute SQL statement: $sql because " . mysql_error($link));
        $countResult = mysql_fetch_array($res);


		$sql = "UPDATE SsoClientSessions SET datechecked = NOW() WHERE
				sessionid = '" . mysql_real_escape_string($sessionId, $link) . "'
				AND clientidentifier = '" . mysql_real_escape_string($this->clientIdentifier, $link) . "'
				AND activated = TRUE
				AND datechecked > '" . $this->getExpiredDate() . "'
		";

		if (!mysql_query($sql, $link))
			die("Could not execute SQL statement: $sql because " . mysql_error($link));
		mysql_close($link);

		return $countResult[0] > 0;
	}

	/**
	 * Remove session from repository. This method should be called as part of the single sign off process.
	 *
	 * @param string $sessionId
	 * @return boolean
	 */
	public function deleteSession($sessionId) {
		$link = $this->getServerConnection();
		$sql = "DELETE FROM SsoClientSessions WHERE
				sessionid = '" . mysql_real_escape_string($sessionId, $link) . "'
				AND clientidentifier = '" . mysql_real_escape_string($this->clientIdentifier, $link) . "'
		";

		if (!mysql_query($sql, $link))
			die("Could not execute SQL statement: $sql because " . mysql_error($link));
		$count = mysql_affected_rows($link);

		mysql_close($link);

		return $count > 0;
	}

	/**
	 * Clean up any expired sessions. This could for example be called on application_start.
	 *
	 * @return void
	 */
	public function cleanUp() {
		$link = $this->getServerConnection();
		$sql = "DELETE FROM SsoClientSessions WHERE
			clientidentifier = '" . mysql_real_escape_string($this->clientIdentifier, $link) . "'
			AND (
				(
					activated = 0
					AND datecreated < '" . $this->getExpiredDate() . "'
				) OR (
					activated = 1
					AND datechecked < '" . $this->getExpiredDate() . "'
				)
			)";

		if (!mysql_query($sql, $link))
			die("Could not execute SQL statement: $sql because " . mysql_error($link));

		mysql_close($link);
	}
}
