<?php

class SherwoodSignOn_SessionRepository_MySqli implements SherwoodSignOn_SessionRepository_Interface {

	private $_host;
	private $_database;
	private $_username;
	private $_password;

	protected $clientIdentifier;
	protected $sessionTimeoutInMinutes;

    /**
     * @inheritdoc
     */
    public function setConfiguration($sessionTimeoutInMinutes, $clientIdentifier, array $configuration = array()) {
        $this->clientIdentifier = $clientIdentifier;

        $this->_host = $configuration['host'];
        $this->_database = $configuration['database'];
        $this->_username = $configuration['username'];
        $this->_password = $configuration['password'];

        $this->sessionTimeoutInMinutes = $sessionTimeoutInMinutes;
	}

	/**
	 * @return string
	 */
	protected function getExpiredDate() {
		$expiredDate = new DateTime();
		$expiredDate->modify("-{$this->sessionTimeoutInMinutes} minutes");
		return $expiredDate->format('Y-m-d H:i:s');
	}

	/**
	 * Registers a new (unactivated) session and returns the session identifier
	 *
	 * @return string
	 */
	public function createSession() {
		$sessionId = uniqid(time());
		$sql = "INSERT INTO SsoClientSessions (sessionid,activated,clientidentifier,datecreated) VALUES (?, 0, ?, NOW())";

		$mysqli = new mysqli($this->_host, $this->_username, $this->_password, $this->_database);
		if (mysqli_connect_errno())
			die("Connection failed:" . mysqli_connect_error());

		$stmt = $mysqli->stmt_init();
		if (!$stmt->prepare($sql))
			die("Could not prepare SQL statement: $sql because $mysqli->error");

		$stmt->bind_param('ss', $sessionId, $this->clientIdentifier);
		$stmt->execute();
		$stmt->close();
		$mysqli->close();

		return $sessionId;
	}

	/**
	 * Validates the session identifier and activates the session.
	 * A session identifier is not valid in this context if the session has already been activated.
	 * This function is called after creating the session. It only activates the session if it's not
	 *
	 * @param string $sessionId Client session identifier
	 * @return boolean True if identifier could be validated and session already exists, otherwise false.
	 */
	public function activateSession($sessionId) {
		$sql = "UPDATE SsoClientSessions SET datechecked = NOW(), activated = TRUE WHERE
				sessionid = ?
				AND clientidentifier = ?
				AND activated = FALSE
				AND datecreated > ?
		";

		$mysqli = new mysqli($this->_host, $this->_username, $this->_password, $this->_database);
		if (mysqli_connect_errno())
			die("Connection failed:" . mysqli_connect_error());

		$stmt = $mysqli->stmt_init();
		if (!$stmt->prepare($sql))
			die("Could not prepare SQL statement: $sql");

		$expiredDate = $this->getExpiredDate();
		$stmt->bind_param('sss', $sessionId, $this->clientIdentifier, $expiredDate);
		$stmt->execute();

		$count = $stmt->affected_rows;

		$stmt->close();
		$mysqli->close();

		return $count > 0;
	}

	/**
	 * Update Session
	 * This function is called every time the user requests this webservice
	 *
	 * @param string $sessionId
	 * @return boolean
	 */
	public function updateSession($sessionId) {
        $sqlCount = "SELECT COUNT(*) FROM SsoClientSessions WHERE
				sessionid = ?
				AND clientidentifier = ?
				AND activated = TRUE
				AND datechecked > ?
		";

		$sqlUpdate = "UPDATE SsoClientSessions SET datechecked = NOW() WHERE
				sessionid = ?
				AND clientidentifier = ?
				AND activated = TRUE
				AND datechecked > ?
		";

        $expiredDate = $this->getExpiredDate();

		$mysqli = new mysqli($this->_host, $this->_username, $this->_password, $this->_database);
		if (mysqli_connect_errno())
			die("Connection failed:" . mysqli_connect_error());

		$stmt = $mysqli->stmt_init();
		if (!$stmt->prepare($sqlCount))
			die ("Could not prepare SQL statement: $sqlCount because $mysqli->error");

		$stmt->bind_param('sss', $sessionId, $this->clientIdentifier, $expiredDate);
		$stmt->execute();

		$countResult = $stmt->get_result()->fetch_array();

		$stmt = $mysqli->stmt_init();
		if (!$stmt->prepare($sqlUpdate))
			die ("Could not prepare SQL statement: $sqlUpdate because $mysqli->error");

		$stmt->bind_param('sss', $sessionId, $this->clientIdentifier, $expiredDate);
		$stmt->execute();

		$stmt->close();
		$mysqli->close();

		return $countResult[0] > 0;
	}

	/**
	 * Remove session from repository. This method should be called as part of the single sign off process.
	 *
	 * @param string $sessionId
	 * @return boolean
	 */
	public function deleteSession($sessionId) {
		$sql = "DELETE FROM SsoClientSessions WHERE sessionid = ? AND clientidentifier = ?";

		$mysqli = new mysqli($this->_host, $this->_username, $this->_password, $this->_database);
		if (mysqli_connect_errno())
			die("Connection failed:" . mysqli_connect_error());

		$stmt = $mysqli->stmt_init();
		if (!$stmt->prepare($sql))
			die ("Could not prepare SQL statement: $sql because $mysqli->error");

		$stmt->bind_param('ss', $sessionId, $this->clientIdentifier);
		$stmt->execute();

		$count = $stmt->affected_rows;

		$stmt->close();
		$mysqli->close();

		return $count > 0;
	}

	/**
	 * Clean up any expired sessions. This could for example be called on application_start.
	 *
	 * @return void
	 */
	public function cleanUp() {
		$sql = "DELETE FROM SsoClientSessions WHERE
			clientidentifier = ?
			AND (
				(
					activated = 0
					AND datecreated < ?
				) OR (
					activated = 1
					AND datechecked < ?
				)
			)";

		$mysqli = new mysqli($this->_host, $this->_username, $this->_password, $this->_database);

		if (mysqli_connect_errno())
			die("Connection failed:" . mysqli_connect_error());

		$stmt = $mysqli->stmt_init();
		if (!$stmt->prepare($sql))
			die ("Could not prepare SQL statement: $sql because $mysqli->error");

		$expiredDate = $this->getExpiredDate();
		$stmt->bind_param('sss', $this->clientIdentifier, $expiredDate, $expiredDate);
		$stmt->execute();
		$stmt->close();
		$mysqli->close();
	}
}
