<?php

interface SherwoodSignOn_SessionRepository_Interface {

	const uniqueKey = 'BrunstadSignon|UserSession'; // Unique prefix for shared repositories

	/**
	 * @param int $sessionTimeoutInMinutes After this amount of seconds, the client-session is outdated/invalid and a
     *                                     new one is created
     * @param string $clientIdentifier Just an identifier for the library-installation
     * @param array $configuration Associative array of strings to configure this session-repository
	 */
	public function setConfiguration($sessionTimeoutInMinutes, $clientIdentifier, array $configuration = array());

	/**
	 * Registers a new (unactivated) session and returns the session identifier
	 *
	 * @return string
	 */
	public function createSession();

	/**
	 * Validates the session identifier and activates the session.
	 * A session identifier is not valid in this context if the session has already been activated.
	 * This function is called after creating the session. It only activates the session if it's not
	 *
	 * @param string $sessionId Client session identifier
	 * @return boolean True if identifier could be validated and session already exists, otherwise false.
	 */
	public function activateSession($sessionId);

	/**
	 * Update Session
	 * This function is called every time the user requests this webservice
	 *
	 * @param string $sessionId
	 * @return boolean
	 */
	public function updateSession($sessionId);

	/**
	 * Remove session from repository. This method should be called as part of the single sign off process.
	 *
	 * @param string $sessionId  Client session identifier
	 * @return boolean
	 */
	public function deleteSession($sessionId);

	/**
	 * Clean up any expired sessions. This could for example be called on application_start.
	 *
	 * @return void
	 */
	public function cleanUp();
}
