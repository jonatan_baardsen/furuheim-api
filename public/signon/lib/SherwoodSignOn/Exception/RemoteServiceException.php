<?php

class SherwoodSignOn_Exception_RemoteServiceException extends ErrorException {

	/**
	 * @var SimpleXMLElement
	 */
	private $_xml;

	/**
	 * @param $xml SimpleXMLElement
	 */
	public function __construct(SimpleXMLElement $xml) {
		parent::__construct($xml->message);

		$this->_xml = $xml;
	}

	/**
	 * @return SimpleXMLElement
	 */
	public function getXml() {
		return $this->_xml;
	}
}
