<?php

class SherwoodSignOn_ClientsideSessionRepository_Cookie implements SherwoodSignOn_ClientsideSessionRepository_Interface {

	private $key;

	/**
	 * @param string $clientCode
	 */
	public function __construct($clientCode) {
		 $this->key = self::uniqueKey . "|" . $clientCode;
	}

	/**
	 * Returns the value of the HttpCookie from the current request / response.
	 * If the application is executed outside of a web context, cookies is returned from a dummy collection
	 *
	 * @param string $name
	 * @return string|void
	 */
	public function getCookie($name) {
		// A cookie that has the value FALSE will be deleted on page reload.
		if (isset($_COOKIE[$name]) && $_COOKIE[$name] !== false)
			return $_COOKIE[$name];
		else
			return null;
	}

	/**
	 * @param string $name
	 * @param string $value
	 * @param int|null $expire
	 * @return void
	 */
	private function setCookie($name, $value, $expire = null) {
		//set a cookie as usual, but ALSO add it to $_COOKIE so the current page load has access
		$_COOKIE[$name] = $value;

		// Dont use this function on command-line as it has no effect and just causes problems in unittests
		if (php_sapi_name() !== 'cli')
			setcookie($name, $value, ($expire !== null) ? time() + $expire : 0, '/');
	}

	/**
	 * @return array
	 */
	public function getSessionData() {
		return explode('|', (string)$this->getCookie($this->key));
	}

	/**
	 * @param array $value
	 * @param null $expire
	 */
	public function setSessionData(array $value, $expire = null) {
		$this->setCookie($this->key, implode('|', $value), $expire);
	}

	/**
	 * @return void
	 */
	public function removeSessionData() {
		$this->setCookie($this->key, false, -3600);
	}
}
