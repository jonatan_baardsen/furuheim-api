<?php

interface SherwoodSignOn_ClientsideSessionRepository_Interface {

    const uniqueKey = 'SherwoodSignOn|ClientSession'; // Unique prefix for shared repositories

    /**
	 * @abstract
	 * @param array $data
	 * @return void
	 */
	public function setSessionData(array $data);

	/**
	 * @abstract
	 * @return array
	 */
	public function getSessionData();

	/**
	 * @abstract
	 * @return void
	 */
	public function removeSessionData();
}
