<?php

/**
 * Class for storing session values
 */
class SherwoodSignOn_ClientsideSessionRepository_Session implements SherwoodSignOn_ClientsideSessionRepository_Interface {

    /**
	 * @return boolean
	 * @codeCoverageIgnore
	 */
	protected function startSession() {
		if (session_id() === "") {
			// If the session is not started until now
			if (headers_sent())
				return false;

			session_start();
		}

		return true;
	}

    /**
     * @param array $data
     * @return void
     */
    public function setSessionData(array $data)
    {
        if ($this->startSession()) {
            $_SESSION[self::uniqueKey] = $data;
        }
    }

    /**
     * @return array
     */
    public function getSessionData()
    {
        if ($this->startSession() && isset($_SESSION[self::uniqueKey]))
            return $_SESSION[self::uniqueKey];
        else
            return array();
    }

    /**
     * @return void
     */
    public function removeSessionData()
    {
        if ($this->startSession()) {
            $_SESSION[self::uniqueKey] = null;
        }
    }
}
