<?php

require_once(dirname(__FILE__) . "/lib/SherwoodSignOn/Autoloader.php");
SherwoodSignOn_Autoloader::getInstance()->register();

// $SignOnRequireSession can be set before including this page to determine
// if unauthenticated users should be redirected to login page or back to the website
// with anonymous session. False (redirect to login page) is default
// Please don't forget to set <defaultDeny> to TRUE in the configuration if you want to give access to everybody.
if (isset($SignOnRequireSession)) {
	$requireLogin = !$SignOnRequireSession;
} else {
	$requireLogin = true;
}

$SignOnClient = new SherwoodSignOn_Client($requireLogin);

if (!$SignOnClient->isUserSignedIn()) {
	$SignOnClient->redirectToSsoServer();
	die();
}

$SignOnProfile = $SignOnClient->getUserProfile();

if ($SignOnClient->getClientSettings()->hasFirewall()) {
    $firewall = new SherwoodSignOn_Firewall($SignOnClient->getClientSettings()->getFirewall());
    if (!$firewall->hasUserAccess($SignOnProfile)) {
        $SignOnClient->localClientSignOut();
        $firewall->redirectToDenyUrl();
        die();
    }
}
